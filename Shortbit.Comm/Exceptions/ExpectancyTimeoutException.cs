﻿using System;
using System.Runtime.Serialization;
using Shortbit.Comm.Messages.Expectancy;
using Shortbit.Utils;

namespace Shortbit.Comm.Exceptions
{
	public static class ExpectancyTimeoutException
	{
		public static ExpectancyTimeoutException<T> Create<T>(MessageExpectancy expectancy, T result)
		{
			Throw.IfNull(expectancy, nameof(expectancy));

			return new ExpectancyTimeoutException<T>(expectancy, result);
		}
		public static ExpectancyTimeoutException<T> Create<T>(Exception innerException, MessageExpectancy expectancy, T result)
		{
			Throw.IfNull(innerException, nameof(innerException));
			Throw.IfNull(expectancy, nameof(expectancy));

			return new ExpectancyTimeoutException<T>(innerException, expectancy, result);
		}
	}

	public class ExpectancyTimeoutException<T> : CommException
	{
		public ExpectancyTimeoutException(MessageExpectancy expectancy, T receivedMessages)
			: this($"Message expectancy {expectancy.GetType().FullName} timed out.", expectancy, receivedMessages)
		{}
		public ExpectancyTimeoutException(Exception innerException, MessageExpectancy expectancy, T receivedMessages)
			: this($"Message expectancy {expectancy.GetType().FullName} timed out.", innerException, expectancy, receivedMessages)
		{}
		public ExpectancyTimeoutException(string message, MessageExpectancy expectancy, T receivedMessages)
		: this(message, null, expectancy, receivedMessages)
		{}
		public ExpectancyTimeoutException(string message, Exception innerException, MessageExpectancy expectancy, T receivedMessages)
			: base(message, innerException)
		{
			this.Expectancy = expectancy;
			this.Timeout = expectancy.Timeout;
			this.ReceivedMessages = receivedMessages;
		}
		
		public ExpectancyTimeoutException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public MessageExpectancy Expectancy { get; }
		public TimeSpan Timeout { get; }
		public T ReceivedMessages { get; }


	}
}
