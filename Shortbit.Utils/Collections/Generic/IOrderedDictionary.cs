﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Collections.Generic
{
	public interface IOrderedDictionary <TKey, TValue> : IDictionary<TKey, TValue>, IOrderedDictionary
	{
		/// <summary>Gets or sets the element at the specified index.</summary>
		/// <returns>The element at the specified index.</returns>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		/// <paramref name="index" /> is less than 0.-or- <paramref name="index" /> is equal to or greater than <see cref="P:System.Collections.ICollection.Count" />. </exception>
		new TValue this[int index] { get; set; }

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Specialized.IOrderedDictionary" /> collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> for the entire <see cref="T:System.Collections.Specialized.IOrderedDictionary" /> collection.</returns>
		new IDictionaryEnumerator GetEnumerator();

		/// <summary>Inserts a key/value pair into the collection at the specified index.</summary>
		/// <param name="index">The zero-based index at which the key/value pair should be inserted.</param>
		/// <param name="key">The object to use as the key of the element to add.</param>
		/// <param name="value">The object to use as the value of the element to add.  The value can be null.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		/// <paramref name="index" /> is less than 0.-or-<paramref name="index" /> is greater than <see cref="P:System.Collections.ICollection.Count" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">An element with the same key already exists in the <see cref="T:System.Collections.Specialized.IOrderedDictionary" /> collection.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Specialized.IOrderedDictionary" /> collection is read-only.-or-The <see cref="T:System.Collections.Specialized.IOrderedDictionary" /> collection has a fixed size.</exception>
		void Insert(int index, TKey key, TValue value);

		/// <summary>
		/// Returns the element with the given index.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		TValue GetByIndex(int index);
		
		/// <summary>
		/// Sets the element with the given index to the given value.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		void SetByIndex(int index, TValue value);

		/// <summary>
		/// Returns the element with the given index.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		TValue GetByKey(TKey key);
		
		/// <summary>
		/// Sets the element with the given index to the given value.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		void SetByKey(TKey key, TValue value);

		/// <summary>
		/// Returns the element with the given index.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		bool TryGetByIndex(int index, out TValue value);

		/// <summary>
		/// Returns the element with the given index.
		/// </summary>
		/// <remarks>
		/// Note that this is an alternate access method for the Index operator in case TKey is of type Int32 (for ambigous call index operator calls).
		/// </remarks>
		bool TryGetByKey(TKey key, out TValue value);
	}
}
