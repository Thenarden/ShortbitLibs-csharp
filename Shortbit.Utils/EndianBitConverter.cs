﻿// /*
//  * EndianBitConverter.cs
//  *
//  *  Created on: 00:42
//  *         Author: 
//  */

using System;
using System.Linq;

namespace Shortbit.Utils
{
	public static class EndianBitConverter
	{
		public static Endianess SystemByteOrder
		{
			get
			{
				if (BitConverter.IsLittleEndian)
					return Endianess.LittleEndian;
				return Endianess.BigEndian;
			}
		}

		/// <summary>
		///     Checks if the Systems byte order differs from the given target order and if so reverses the data byte array.
		/// </summary>
		/// <param name="data">The data array. Note that if no reverse takes place, the same array reference is returned.</param>
		/// <param name="targetOrder">The target byte order.</param>
		/// <returns>
		///     Returns the byte array in the given order. Note that on reversing a new instance is generated, otherwise the
		///     same instance ref. is returned.
		/// </returns>
		public static byte[] CheckReverse(byte[] data, Endianess targetOrder)
		{
			if (SystemByteOrder == targetOrder)
				return data;

			return data.Reverse().ToArray();
		}

		public static byte[] GetBytes(bool value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(char value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(short value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(int value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(long value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(ushort value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(uint value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(ulong value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(float value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static byte[] GetBytes(double value, Endianess order)
		{
			return CheckReverse(BitConverter.GetBytes(value), order);
		}

		public static char ToChar(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToChar(CheckReverse(value, order), startIndex);
		}

		public static short ToInt16(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToInt16(CheckReverse(value, order), startIndex);
		}

		public static int ToInt32(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToInt32(CheckReverse(value, order), startIndex);
		}

		public static long ToInt64(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToInt64(CheckReverse(value, order), startIndex);
		}

		public static ushort ToUInt16(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToUInt16(CheckReverse(value, order), startIndex);
		}

		public static uint ToUInt32(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToUInt32(CheckReverse(value, order), startIndex);
		}

		public static ulong ToUInt64(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToUInt64(CheckReverse(value, order), startIndex);
		}

		public static float ToSingle(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToSingle(CheckReverse(value, order), startIndex);
		}

		public static double ToDouble(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToDouble(CheckReverse(value, order), startIndex);
		}

		public static string ToString(byte[] value, int startIndex, int length, Endianess order)
		{
			return BitConverter.ToString(CheckReverse(value, order), startIndex, length);
		}

		public static string ToString(byte[] value, Endianess order)
		{
			return BitConverter.ToString(CheckReverse(value, order));
		}

		public static string ToString(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToString(CheckReverse(value, order), startIndex);
		}

		public static bool ToBoolean(byte[] value, int startIndex, Endianess order)
		{
			return BitConverter.ToBoolean(CheckReverse(value, order), startIndex);
		}
	}
}