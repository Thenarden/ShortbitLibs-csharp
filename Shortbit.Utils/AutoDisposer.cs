﻿// /*
//  * AutoDisposer.cs
//  *
//  *  Created on: 09:17
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Shortbit.Utils
{
	public static class AutoDisposer
	{
		private static Dictionary<Type, DisposeableClassElements> registry = new Dictionary<Type, DisposeableClassElements>();

		private class DisposeableClassElements
		{
			public List<MemberInfo> Members;
			public List<FieldInfo> Fields;

			public DisposeableClassElements(List<MemberInfo> members, List<FieldInfo> fields)
			{
				Members = members;
				Fields = fields;
			}
		}

		private static void ReadDisposeableElements(Type t)
		{
			if (registry.ContainsKey(t))
				return;

			List<FieldInfo> fields =
				t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)
					.Where(ft => typeof (IDisposable).IsAssignableFrom(ft.FieldType))
					.ToList();
			List<MemberInfo> members =
				t.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)
					.Where(mt => typeof (IDisposable).IsAssignableFrom(mt.DeclaringType))
					.ToList();

			registry.Add(t, new DisposeableClassElements(members, fields));
		}

		public static void DisposeFields(Object disposable)
		{
			if (disposable == null) return;

			ReadDisposeableElements(disposable.GetType());

			DisposeableClassElements elements = registry[disposable.GetType()];

			foreach (FieldInfo field in elements.Fields)
			{
				var value = field.GetValue(disposable) as IDisposable;

				value?.Dispose();
			}
		}
	}
}