﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Comm.Messages
{
	public enum DispatchWaitPolicy
	{
		DontWait,
		WaitForAny,
		WaitForAll
	}
}
