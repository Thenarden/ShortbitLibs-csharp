﻿// /*
//  * C_RSA.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Shortbit.Data.Cryptography
{
    [Obsolete("This class got obsolete. Please use new RSA implementation.")]
    public class C_RSA : C_Base
    {
        public const int MaxEncryptionData = 64; // Eigentlich 245, aber aus Sicherheitsgründen...

        private RSACryptoServiceProvider RSA_Decrypter; // Instance for encrypting data with generated key
        private RSACryptoServiceProvider RSA_Encrypter; // Instance for encrypting data with given key

        public C_RSA()
        {
            RSA_Encrypter = new RSACryptoServiceProvider();
            RSA_Decrypter = new RSACryptoServiceProvider();

            GenerateNewKey();
        }

        public Byte[] Key
        {
            get // Returns the (only public) Key for encryption by taking it from decryption instance
            {
                RSAParameters param = RSA_Decrypter.ExportParameters(false); // never show private keys, too

                var r = new Byte[param.Exponent.Length + param.Modulus.Length + 1];

                r[0] = (Byte) param.Exponent.Length;
                for (int i = 0; i < param.Exponent.Length; i++) // 3
                    r[i + 1] = param.Exponent[i];
                for (int i = 0; i < param.Modulus.Length; i++)
                    r[param.Exponent.Length + i + 1] = param.Modulus[i];

                return r;
            }
            set // Sets the (only public) Key for encryption 
            {
                if (value.Length <= 0)
                    return;
                var Exponent = new Byte[value[0]];
                var Modulus = new Byte[value.Length - Exponent.Length - 1];

                for (int i = 0; i < Exponent.Length; i++)
                    Exponent[i] = value[i + 1];
                for (int i = 0; i < Modulus.Length; i++)
                    Modulus[i] = value[Exponent.Length + i + 1];

                var param = new RSAParameters();
                param.Exponent = Exponent;
                param.Modulus = Modulus;
                RSA_Encrypter.ImportParameters(param);
            }
        }

        public Byte[] Encrypt(Byte[] _Data)
        {
            //	if (_Data.Length > MaxEncryptionData)
            //		throw new Exception("You can't encrypt data larger than " + MaxEncryptionData.ToString() + " Bytes.");

            var Data = new List<byte>();
            Data.AddRange(_Data);

            List<List<Byte>> partedData = GetParts(Data, MaxEncryptionData);
            var r = new List<byte>();

            bool First = true;
            int bytesPerPart = 0;

            foreach (List<Byte> part in partedData)
            {
                Byte[] encry = RSA_Encrypter.Encrypt(part.ToArray(), false);
                r.AddRange(encry);

                if (First)
                    bytesPerPart = encry.Length;
                First = false;
            }
            r.InsertRange(0, BitConverter.GetBytes(bytesPerPart));
            return r.ToArray();
        }

        public Byte[] Decrypt(Byte[] _Data)
        {
            var Data = new List<byte>();
            Data.AddRange(_Data);

            int bytesPerPart = BitConverter.ToInt32(Data.GetRange(0, 4).ToArray(), 0);
            Data.RemoveRange(0, 4);

            List<List<Byte>> partedData = GetParts(Data, bytesPerPart);
            var r = new List<byte>();

            foreach (List<Byte> part in partedData)
            {
                Byte[] encry = RSA_Decrypter.Decrypt(part.ToArray(), false);
                r.AddRange(encry);
            }
            return r.ToArray();
        }

        public C_Base Clone()
        {
            C_Base r = new C_RSA();
            r.Key = Key;
            return r;
        }

        public void GenerateNewKey()
        {
            RSA_Decrypter = new RSACryptoServiceProvider();
            RSA_Encrypter.ImportParameters(RSA_Decrypter.ExportParameters(false)); // Maybe not...
        }

        private List<List<Byte>> GetParts(List<Byte> Data, int bytesPerPart)
        {
            var partedData = new List<List<byte>>();
            while (Data.Count > 0)
            {
                var part = new List<byte>();

                if (Data.Count <= bytesPerPart)
                {
                    part.AddRange(Data);
                    Data.Clear();
                }
                else
                {
                    var bytes = new Byte[bytesPerPart];
                    for (int i = 0; i < bytes.Length; i++)
                        bytes[i] = Data[i];
                    part.AddRange(bytes);
                    Data.RemoveRange(0, bytesPerPart);
                }
                partedData.Add(part);
            }
            return partedData;
        }
    }
}