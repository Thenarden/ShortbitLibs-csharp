﻿using System;

namespace Shortbit.Comm.Test
{
	class TestDataPacket : BaseDataPacket
	{
		public TestDataPacket(bool _IsResponse, Int64 _ID, Object _Command, Object _Data)
			: base (_IsResponse, _ID, _Command, _Data)	
		{
		}
	}
}
