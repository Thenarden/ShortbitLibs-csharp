﻿// /*
//  * ReadFullyStream.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.IO;

namespace Shortbit.Utils.IO
{
	/// <summary>
	/// </summary>
	/// <version>2006/10/13 1.0</version>
	/// <license>
	///     NAudio is an open source .NET audio library written by Mark Heath (mark.heath@gmail.com)
	///     For more information, visit http://naudio.codeplex.com
	///     Microsoft Public License (Ms-PL)
	///     This license governs use of the accompanying software. If you use the software, you accept this license. If you do
	///     not accept the license, do not use the software.
	///     1. Definitions
	///     The terms "reproduce," "reproduction," "derivative works," and "distribution" have the same meaning here as under
	///     U.S. copyright law.
	///     A "contribution" is the original software, or any additions or changes to the software.
	///     A "contributor" is any person that distributes its contribution under this license.
	///     "Licensed patents" are a contributor's patent claims that read directly on its contribution.
	///     2. Grant of Rights
	///     (A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in
	///     section 3, each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its
	///     contribution, prepare derivative works of its contribution, and distribute its contribution or any derivative works
	///     that you create.
	///     (B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section
	///     3, each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make,
	///     have made, use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or
	///     derivative works of the contribution in the software.
	///     3. Conditions and Limitations
	///     (A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or
	///     trademarks.
	///     (B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software,
	///     your patent license from such contributor to the software ends automatically.
	///     (C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, and
	///     attribution notices that are present in the software.
	///     (D) If you distribute any portion of the software in source code form, you may do so only under this license by
	///     including a complete copy of this license with your distribution. If you distribute any portion of the software in
	///     compiled or object code form, you may only do so under a license that complies with this license.
	///     (E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties,
	///     guarantees or conditions. You may have additional consumer rights under your local laws which this license cannot
	///     change. To the extent permitted under your local laws, the contributors exclude the implied warranties of
	///     merchantability, fitness for a particular purpose and non-infringement.
	/// </license>
	public class ReadFullyStream : Stream
	{
		private readonly Stream sourceStream;
		private long pos; // psuedo-position
		private readonly byte[] readAheadBuffer;
		private int readAheadLength;
		private int readAheadOffset;

		public ReadFullyStream(Stream sourceStream)
		{
			this.sourceStream = sourceStream;
			readAheadBuffer = new byte[4096];
		}

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return false; }
		}

		public override bool CanWrite
		{
			get { return false; }
		}

		public override void Flush()
		{
			throw new InvalidOperationException();
		}

		public override long Length
		{
			get { return pos; }
		}

		public override long Position
		{
			get { return pos; }
			set { throw new InvalidOperationException(); }
		}


		public override int Read(byte[] buffer, int offset, int count)
		{
			int bytesRead = 0;
			while (bytesRead < count)
			{
				int readAheadAvailableBytes = readAheadLength - readAheadOffset;
				int bytesRequired = count - bytesRead;
				if (readAheadAvailableBytes > 0)
				{
					int toCopy = Math.Min(readAheadAvailableBytes, bytesRequired);
					Array.Copy(readAheadBuffer, readAheadOffset, buffer, offset + bytesRead, toCopy);
					bytesRead += toCopy;
					readAheadOffset += toCopy;
				}
				else
				{
					readAheadOffset = 0;
					readAheadLength = sourceStream.Read(readAheadBuffer, 0, readAheadBuffer.Length);
					//Debug.WriteLine(String.Format("Read {0} bytes (requested {1})", readAheadLength, readAheadBuffer.Length));
					if (readAheadLength == 0)
					{
						break;
					}
				}
			}
			pos += bytesRead;
			return bytesRead;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new InvalidOperationException();
		}

		public override void SetLength(long value)
		{
			throw new InvalidOperationException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new InvalidOperationException();
		}
	}
}