﻿// /*
//  * NBTCompound.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using Shortbit.Data.Serialization;
using Shortbit.Utils;

namespace Shortbit.Data.NBT
{
    public class NBTCompound : NBTTag, IEnumerable<KeyValuePair<string, NBTTag>>
    {
	    public static NBTCompound FromObject(object value)
	    {
			var res = new NBTCompound();

			Dictionary<string, object> props = value.GetType()
				.GetProperties()
				.ToDictionary(x => x.Name, x => x.GetValue(value, null));
			foreach (KeyValuePair<string, object> pair in props)
			{
				res.Add(pair.Key, NBTTag.FromValue(pair.Value));
			}
		    return res;
	    }

        private readonly Dictionary<string, NBTTag> components;

        public NBTCompound()
        {
            components = new Dictionary<string, NBTTag>();
        }

        public NBTCompound(IDictionary<String, NBTTag> items)
        {
            components = new Dictionary<string, NBTTag>(items);
        }
		
        public NBTCompound(SerializationReader reader)
        {
            components = new Dictionary<string, NBTTag>();

            int num = reader.ReadInt32();
            //Log.Debug("Deserializing NBTCompound with " + num + " entries");

            for (int i = 0; i < num; i++)
            {
                String key = reader.ReadString();
                //Log.Debug("Deserializing element \"" + key + "\"");
                var type = (NBTType) reader.ReadInt32();

                Set(key, Deserialize(type, reader));
            }
        }

        public NBTCompound(XmlElement element)
        {
            components = new Dictionary<string, NBTTag>();

            IEnumerable<XmlElement> children = element.GetElementsByTagName("entry").Cast<XmlElement>();
            var xmlElements = children as IList<XmlElement> ?? children.ToList();
            //Log.Debug("Decomposing NBTCompound with " + xmlElements.Count() + " entries");

            foreach (XmlElement entry in xmlElements)
            {
                string key = entry.GetAttribute("key");

                XmlElement content = entry.FirstChildElement();
                Set(key, Deserialize(content));
            }
        }

        public IEnumerator<KeyValuePair<string, NBTTag>> GetEnumerator()
        {
            return components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override void Serialize(SerializationWriter writer)
        {
           // Log.Debug("Serializing NBTCompound with " + components.Count + " entries");
            writer.Write(components.Count);
            foreach (KeyValuePair<String, NBTTag> pair in components)
            {
               // Log.Debug("Serializing element \"" + pair.Key + "\"");
                writer.Write(pair.Key);
                writer.Write((Int32) pair.Value.Type);
                pair.Value.Serialize(writer);
            }
        }

        public override XmlElement Serialize(XmlDocument doc)
        {
            XmlElement eBase = doc.CreateElement(Type.ToString());

            foreach (KeyValuePair<String, NBTTag> child in this)
            {
                XmlElement eEntry = doc.CreateElement("entry");
                eEntry.SetAttribute("key", child.Key);

                XmlElement eChild = child.Value.Serialize(doc);
                eEntry.AppendChild(eChild);
                eBase.AppendChild(eEntry);
            }

            return eBase;
        }

        public override object GetValue()
        {
            return components.ToDictionary(pair => pair.Key, pair => pair.Value.GetValue());
        }

        public object GetValue(String key)
        {
            return Get(key).GetValue();
        }

        public T GetValue<T>(String key)
        {
            return Get(key).GetValue<T>();
        }
		

        public void Set(string key, NBTTag value)
        {
            if (components.ContainsKey(key))
                components[key] = value;
            else
                components.Add(key, value);
        }

        public T Get<T>(String key) where T : NBTTag
        {
            NBTTag tag = Get(key);
            //	if (!typeof(T).IsAssignableFrom(tag.GetType()))
            //		throw new ArgumentException("Given Typename is not assignable from this component value."); // Exception will get thrown by Cast. No ned for this...

            return (T) tag;
        }

        public NBTTag Get(String key)
        {
            if (!components.ContainsKey(key))
                throw new ArgumentException("NBTCompound does not contain key " + key);
            return components[key];
        }

        public void Add(KeyValuePair<string, NBTTag> pair)
        {
            Set(pair.Key, pair.Value);
        }

        public void Add(string key, NBTTag value)
        {
            Set(key, value);
        }

        public NBTCompound Combine(NBTCompound other)
        {
            foreach (KeyValuePair<string, NBTTag> pair in other)
                Add(pair);
            return this;
        }


        public String[] GetKeys()
        {
            return components.Keys.ToArray();
        }


        /*	public static implicit operator NBTCompound(object data)
		{
			return new NBTCompound(data);
		}
	 */
    }
}