﻿// /*
//  * StringExtension.cs
//  *
//  *  Created on: 16:30
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace Shortbit.Utils
{
	public static class StringExtension
	{
		public static bool ContainsAny(this string haystack, params IEnumerable<string>[] needles)
		{
			return needles.Any(ns => ns.Any(haystack.Contains));
		}

		public static bool EqualsAny(this string haystack, params IEnumerable<string>[] needles)
		{
			return needles.Any(ns => ns.Any(haystack.Equals));
		}

		public static bool ContainsAll(this string haystack, params IEnumerable<string>[] needles)
		{
			return needles.All(ns => ns.Any(haystack.Contains));
		}


	    public static string EscapeXml(this string me)
	    {
            var returnString = me;

            returnString = returnString.Replace("&", "&amp;");
            returnString = returnString.Replace("'", "&apos;");
            returnString = returnString.Replace("\"", "&quot;");
            returnString = returnString.Replace(">", "&gt;");
            returnString = returnString.Replace("<", "&lt;");

	        return returnString;
	    }

	    public static string UnescapeXml(this string me)
        {
            var returnString = me;

            returnString = returnString.Replace("&apos;", "'");
            returnString = returnString.Replace("&quot;", "\"");
            returnString = returnString.Replace("&gt;", ">");
            returnString = returnString.Replace("&lt;", "<");
            returnString = returnString.Replace("&amp;", "&");

            return returnString;
        }

		
		public static int IndexOfAfter(this string self, char value)
		{
			var idx = self.IndexOf(value);

			if (idx >= 0)
				idx += 1;
			return idx;
		}
		public static int IndexOfAfter(this string self, char value, int startIndex)
		{
			var idx = self.IndexOf(value, startIndex);

			if (idx >= 0)
				idx += 1;
			return idx;
		}
		public static int IndexOfAfter(this string self, char value, int startIndex, int count)
		{
			var idx = self.IndexOf(value, startIndex, count);

			if (idx >= 0)
				idx += 1;
			return idx;
		}

		public static int IndexOfAfter(this string self, string value, StringComparison comparison)
		{
			var idx = self.IndexOf(value, comparison);

			if (idx >= 0)
				idx += value.Length;
			return idx;
		}
		public static int IndexOfAfter(this string self, string value, int startIndex, StringComparison comparison)
		{
			var idx = self.IndexOf(value, startIndex, comparison);

			if (idx >= 0)
				idx += value.Length;
			return idx;
		}
		public static int IndexOfAfter(this string self, string value, int startIndex, int count, StringComparison comparison)
		{
			var idx = self.IndexOf(value, startIndex, count, comparison);

			if (idx >= 0)
				idx += value.Length;
			return idx;
		}


		public static IEnumerable<string> Split(this string str,
			Func<char, bool> controller)
		{
			return str.Split(controller, StringSplitOptions.None);
		}
		public static IEnumerable<string> Split(this string str,
											Func<char, bool> controller, StringSplitOptions options)
		{
			int nextPiece = 0;

			for (int c = 0; c < str.Length; c++)
			{
				if (controller(str[c]))
				{
					var part = str.Substring(nextPiece, c - nextPiece);
					nextPiece = c + 1;
					if (!string.IsNullOrEmpty(part) || options == StringSplitOptions.None)
						yield return part;
				}
			}

			var lastPart = str.Substring(nextPiece);
			if (!string.IsNullOrEmpty(lastPart) || options == StringSplitOptions.None)
				yield return lastPart;
		}

		public static string TrimMatchingQuotes(this string input, char quote)
		{
			if ((input.Length >= 2) &&
				(input[0] == quote) && (input[input.Length - 1] == quote))
				return input.Substring(1, input.Length - 2);

			return input;
		}

		public static IEnumerable<string> SplitCommandLine(this string commandLine)
		{
			bool inQuotes = false;
			bool prevEscape = false;

			return commandLine.Split(c =>
			{
				if (c == '\"' && !prevEscape)
					inQuotes = !inQuotes;

				if (c == '\\')
					prevEscape = true;
				else
					prevEscape = false;
				
				return !inQuotes && c == ' ';
			}, StringSplitOptions.RemoveEmptyEntries)
			.Select(line => 
			{
				var builder = new StringBuilder(line);
				
				int i = 0;
				while (i < builder.Length)
				{
					if (builder[i] == ' ' && i == 0)
					{
						builder.Remove(0, 1);
						continue;
					}

					if (builder[i] == '"')
					{
						if (i == 0 || builder[i - 1] != '\\')
						{
							builder.Remove(i, 1);
							continue;
						}
						if (i > 0)
						{
							builder.Remove(i - 1, 1);
							continue;
						}
					}
					i++;
				}
					
				while (builder[builder.Length - 1] == ' ')
					builder.Remove(builder.Length - 1, 1);
				
				return builder.ToString();
			}).Where(arg => !string.IsNullOrEmpty(arg));
		}

		public static string ToSnakeCase(this string self)
		{
			if (string.IsNullOrEmpty(self))
                return self;
			

			var builder = new StringBuilder(self.Length + Math.Min(2, self.Length / 5));
			var previousCategory = default(UnicodeCategory?);
			for (var currentIndex = 0; currentIndex < self.Length; currentIndex++)
            {
                var currentChar = self[currentIndex];
                if (currentChar == '_')
                {
                    builder.Append('_');
                    previousCategory = null;
                    continue;
                }

                var currentCategory = char.GetUnicodeCategory(currentChar);
                switch (currentCategory)
                {
                    case UnicodeCategory.UppercaseLetter:
                    case UnicodeCategory.TitlecaseLetter:
                        if (previousCategory == UnicodeCategory.SpaceSeparator ||
                            previousCategory == UnicodeCategory.LowercaseLetter ||
                            previousCategory != UnicodeCategory.DecimalDigitNumber &&
                            previousCategory != null &&
                            currentIndex > 0 &&
					currentIndex + 1 < self.Length &&
                            char.IsLower(self[currentIndex + 1]))
                        {
                            builder.Append('_');
                        }

                        currentChar = char.ToLower(currentChar, CultureInfo.InvariantCulture);
                        break;

                    case UnicodeCategory.LowercaseLetter:
                    case UnicodeCategory.DecimalDigitNumber:
                        if (previousCategory == UnicodeCategory.SpaceSeparator)
                        {
                            builder.Append('_');
                        }
                        break;

                    default:
                        if (previousCategory != null)
                        {
                            previousCategory = UnicodeCategory.SpaceSeparator;
                        }
                        continue;
                }

                builder.Append(currentChar);
                previousCategory = currentCategory;
            }

            return builder.ToString();
		}

		public static string ToCamelCase(this string self, bool firstUpperCase = false)
		{
			if (string.IsNullOrEmpty(self))
				return self;

			var regex = new Regex(@"[\s_]+");

			var parts = regex.Split(self);

			bool first = true;
			var builder = new StringBuilder();
			foreach ( var part in parts)
			{
				if (part.Length <= 1)
				{
					builder.Append(part);
					first = false;
					continue;
				}

				if (!first || firstUpperCase)
					builder.Append(part.Remove(1).ToUpperInvariant());
				else
					builder.Append(part.Remove(1).ToLowerInvariant());
				builder.Append(part.Substring(1));

				first = false;
			}

			return builder.ToString();
		}

		public static string ToPascalCase(this string self) => ToCamelCase(self, true);

		public static string ToUpperFirst(this string self) => self.Substring(0, 1).ToUpperInvariant() + self.Substring(1);
		public static string ToLowerFirst(this string self) => self.Substring(0, 1).ToLowerInvariant() + self.Substring(1);
		
	}
}