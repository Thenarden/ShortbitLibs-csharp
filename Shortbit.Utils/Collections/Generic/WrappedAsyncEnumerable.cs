﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;


#if NETCOREAPP
namespace Shortbit.Utils.Collections.Generic
{
	public readonly struct WrappedAsyncEnumerable<T> : IAsyncEnumerable<T>, IEnumerable<T>
	{
		private readonly IEnumerable<T> enumerable;
		public WrappedAsyncEnumerable(IEnumerable<T> enumerable)
		{
			this.enumerable = enumerable;
		}

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.enumerable.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default)
		{
			return new WrappedAsyncEnumerator<T>(this.enumerable.GetEnumerator());
		}
	}
}
#endif