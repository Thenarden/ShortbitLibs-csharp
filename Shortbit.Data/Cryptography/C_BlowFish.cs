﻿// /*
//  * C_BlowFish.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Security.Cryptography;

namespace Shortbit.Data.Cryptography
{
    public class C_BlowFish : C_Base
    {
        private RNGCryptoServiceProvider randomSource;

        public C_BlowFish()
        {
            randomSource = new RNGCryptoServiceProvider();
            GenerateNewKey();
        }

        public byte[] Key { get; set; }

        public byte[] Encrypt(byte[] input)
        {
            if (input.Length == 0)
                return new Byte[1] {0};

            BlowFish fish = setupBlowfish();

            Byte[] encoded = fish.Encrypt_CBC(input);

            int inputLength = input.Length;
            int outputLength = encoded.Length;

            int paddedBytes = outputLength - inputLength;

            var output = new Byte[encoded.Length + 1];
            Buffer.BlockCopy(encoded, 0, output, 1, encoded.Length);

            output[0] = (byte) paddedBytes;

            return output;
        }

        public byte[] Decrypt(byte[] input)
        {
            if (input == null)
                return null;
            if (input.Length == 0)
                return new Byte[0];

            uint paddedBytes = input[0];

            var inputLength = (uint) input.Length;
            uint outputLength = inputLength - paddedBytes;
            Byte[] encoded = null;

            encoded = new Byte[input.Length - 1];
            Buffer.BlockCopy(input, 1, encoded, 0, encoded.Length);

            BlowFish fish = setupBlowfish();
            Byte[] decoded = fish.Decrypt_CBC(encoded);
            Byte[] output = null;

            if (paddedBytes == 0)
            {
                output = decoded;
            }
            else
            {
                output = new Byte[decoded.Length - paddedBytes];
                Buffer.BlockCopy(decoded, 0, output, 0, output.Length);
            }

            return output;
        }

        public C_Base Clone()
        {
            C_Base r = new C_BlowFish();
            r.Key = Key;
            return r;
        }

        public void GenerateNewKey()
        {
            if (Key == null)
                Key = new byte[56 + 8];
            randomSource.GetBytes(Key);
        }

        private BlowFish setupBlowfish()
        {
            var key = new Byte[56];

            Buffer.BlockCopy(Key, 0, key, 0, key.Length);

            var IV = new byte[8];
            Buffer.BlockCopy(Key, key.Length, IV, 0, IV.Length);


            var ret = new BlowFish(key);
            ret.IV = IV;

            return ret;
        }
    }
}