﻿using System;

namespace Shortbit.Data.Serialization
{


    public interface IFormatter<TS>
    {
	    TS CreateEmpty(Type targetType);

        TS Format(object value);

        object Parse(TS value, Type targetType);

    }
    public interface IFormatter<TS, T> : IFormatter<TS>
	{
		TS CreateEmpty();

		TS Format(T value);

        T Parse(TS value);
    }


	// ReSharper disable once InconsistentNaming
	public static class IFormatterExtension
	{
		public static object ParseElement<TS>(this IFormatter<TS> self, SerializedElement<TS> element, Type targetType)
		{
			return self.Parse(element.LoadValue(self.CreateEmpty(targetType)), targetType);
		}
		public static T ParseElement<TS, T>(this IFormatter<TS, T> self, SerializedElement<TS> element)
		{
			return self.Parse(element.LoadValue(self.CreateEmpty()));
		}
	}
}
