﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization.Format
{
    public static class StringFormatters
    {
        private static readonly Dictionary<Type, IFormatter<string>> stringFormatters = new Dictionary<Type, IFormatter<string>>()
        {
            {typeof(Boolean), new BooleanStringFormat() },
            {typeof(String), new StringStringFormat() },
            {typeof(Single), new SingleStringFormat() },
            {typeof(Double), new DoubleStringFormat() },
            {typeof(Decimal), new DecimalStringFormat() },
            {typeof(Byte), new ByteStringFormat() },
            {typeof(SByte), new SByteStringFormat() },
            {typeof(Int16), new Int16StringFormat() },
            {typeof(UInt16), new UInt16StringFormat() },
            {typeof(Int32), new Int32StringFormat() },
            {typeof(UInt32), new UInt32StringFormat() },
            {typeof(Int64), new Int64StringFormat() },
            {typeof(UInt64), new UInt64StringFormat() },
        };

        public static void Register(Type primitiveType, IFormatter<string> formatter)
        {
            if (!PrimitiveTypes.Contains(primitiveType))
                throw new ArgumentException($"Type {primitiveType.FullName} is not a primitive type.");
            stringFormatters[primitiveType] = formatter;
        }

        public static IFormatter<string> Get(Type primitiveType)
        {
            if (!PrimitiveTypes.Contains(primitiveType))
                throw new ArgumentException($"Type {primitiveType.FullName} is not a primitive type.");

            if (!stringFormatters.ContainsKey(primitiveType))
                throw new ArgumentException($"Primitive type {primitiveType.FullName} has no assigned formatter. This is a bug!");

            return stringFormatters[primitiveType];
        }
    }

    #region String Formatters

    internal class BooleanStringFormat : WrappedFormat<string, Boolean>
    {
        public override string Format(Boolean value)
        {
            return value.ToString();
        }

        public override Boolean Parse(string value)
        {
            return Convert.ToBoolean(value);
        }
    }
    internal class StringStringFormat : WrappedFormat<string, String>
    {
        public override string Format(String value)
        {
            return value;
        }

        public override String Parse(string value)
        {
            return value;
        }
    }

    internal class SingleStringFormat : WrappedFormat<string, Single>
    {
        public override string Format(Single value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public override Single Parse(string value)
        {
            return Convert.ToSingle(value);
        }
    }

    internal class DoubleStringFormat : WrappedFormat<string, Double>
    {
        public override string Format(Double value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public override Double Parse(string value)
        {
            return Convert.ToDouble(value);
        }
    }

    internal class DecimalStringFormat : WrappedFormat<string, Decimal>
    {
        public override string Format(Decimal value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public override Decimal Parse(string value)
        {
            return Convert.ToDecimal(value);
        }
    }

    internal class ByteStringFormat : WrappedFormat<string, Byte>
    {
        public override string Format(Byte value)
        {
            return value.ToString();
        }

        public override Byte Parse(string value)
        {
            return Convert.ToByte(value);
        }
    }

    internal class SByteStringFormat : WrappedFormat<string, SByte>
    {
        public override string Format(SByte value)
        {
            return value.ToString();
        }

        public override SByte Parse(string value)
        {
            return Convert.ToSByte(value);
        }
    }

    internal class Int16StringFormat : WrappedFormat<string, Int16>
    {
        public override string Format(Int16 value)
        {
            return value.ToString();
        }

        public override Int16 Parse(string value)
        {
            return Convert.ToInt16(value);
        }
    }

    internal class UInt16StringFormat : WrappedFormat<string, UInt16>
    {
        public override string Format(UInt16 value)
        {
            return value.ToString();
        }

        public override UInt16 Parse(string value)
        {
            return Convert.ToUInt16(value);
        }
    }

    internal class Int32StringFormat : WrappedFormat<string, Int32>
    {
        public override string Format(Int32 value)
        {
            return value.ToString();
        }

        public override Int32 Parse(string value)
        {
            return Convert.ToInt32(value);
        }
    }

    internal class UInt32StringFormat : WrappedFormat<string, UInt32>
    {
        public override string Format(UInt32 value)
        {
            return value.ToString();
        }

        public override UInt32 Parse(string value)
        {
            return Convert.ToUInt32(value);
        }
    }

    internal class Int64StringFormat : WrappedFormat<string, Int64>
    {
        public override string Format(Int64 value)
        {
            return value.ToString();
        }

        public override Int64 Parse(string value)
        {
            return Convert.ToInt64(value);
        }
    }

    internal class UInt64StringFormat : WrappedFormat<string, UInt64>
    {
        public override string Format(UInt64 value)
        {
            return value.ToString();
        }

        public override UInt64 Parse(string value)
        {
            return Convert.ToUInt64(value);
        }
    }

    #endregion
}
