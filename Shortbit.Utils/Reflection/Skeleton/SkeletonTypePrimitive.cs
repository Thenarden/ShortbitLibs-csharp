﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
#if !NET40
	public sealed class SkeletonTypePrimitive : SkeletonType
	{
		public enum PrimitiveType
		{

			SByte,
			Int16,
			Int32,
			Int64,
			Byte,
			UInt16,
			UInt32,
			UInt64,
			
			Single,
			Double, 
			Decimal,
			
			Boolean,
			String,
			DateTime,
			TimeSpan,
		}

		private static readonly Dictionary<Type, PrimitiveType> simpleTypesSwitch = new Dictionary<Type, PrimitiveType>
		{
			{typeof(SByte), PrimitiveType.SByte},
			{typeof(Int16), PrimitiveType.Int16},
			{typeof(Int32), PrimitiveType.Int32},
			{typeof(Int64), PrimitiveType.Int64},
			{typeof(Byte), PrimitiveType.Byte},
			{typeof(UInt16), PrimitiveType.UInt16},
			{typeof(UInt32), PrimitiveType.UInt32},
			{typeof(UInt64), PrimitiveType.UInt64},
			
			{typeof(Single), PrimitiveType.Single},
			{typeof(Double), PrimitiveType.Double},
			{typeof(Decimal), PrimitiveType.Decimal},
			
			{typeof(Boolean), PrimitiveType.Boolean},
			{typeof(String), PrimitiveType.String},
			{typeof(DateTime), PrimitiveType.DateTime},
			{typeof(TimeSpan), PrimitiveType.TimeSpan},

		};

		public static bool IsPrimitiveType(Type type) => type.IsPrimitive || type == typeof(string) || type == typeof(DateTime) || type == typeof(TimeSpan);
		// => SkeletonTypeSimple.simpleTypesSwitch.ContainsKey(type);

		public static SkeletonTypePrimitive CreatePrimitive(Type sourceType)
			=> SkeletonTypePrimitive.CreatePrimitive(new SkeletonTypeCreationContext(), sourceType);

		public static SkeletonTypePrimitive CreatePrimitive(SkeletonTypeCreationContext context, Type sourceType)
			=> SkeletonTypePrimitive.CreatePrimitive(context, sourceType, true);

		internal static SkeletonTypePrimitive CreatePrimitive(SkeletonTypeCreationContext context, Type sourceType, bool typeCheck )
		{
			if (typeCheck && !SkeletonTypePrimitive.IsPrimitiveType(sourceType))
				throw new ArgumentException($"Given type \"{sourceType.FullName}\" is not a simple type.");

			if (context.Cache.ContainsKey(sourceType))
				return (SkeletonTypePrimitive) context.Cache[sourceType];
			
			var type = SkeletonTypePrimitive.simpleTypesSwitch[sourceType];

			var res = new SkeletonTypePrimitive(sourceType, type);
			context.Cache.Add(res);
			return res;
		}

		private SkeletonTypePrimitive(Type sourceType, PrimitiveType primitive) : base(sourceType)
		{
			this.Primitive = primitive;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString() => $"Primitive <{this.Primitive}>";

		[NotNull]
		public PrimitiveType Primitive { get; }
	}
	#endif
}
