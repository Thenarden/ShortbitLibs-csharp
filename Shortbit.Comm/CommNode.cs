﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using QuickGraph.Graphviz;
using Shortbit.Comm.CommLibMessages;
using Shortbit.Comm.LinkStateNetwork;
using Shortbit.Comm.Messages;
using Shortbit.Comm.Messages.Expectancy;
using Shortbit.Comm.Messages.Expectancy.Fluent;
using Shortbit.Comm.Transport;
using Shortbit.Data.Serialization;
using Shortbit.Utils;
using Shortbit.Utils.AsyncCallbacks;
using Shortbit.Utils.Properties;

namespace Shortbit.Comm
{
	public class CommNode
	{

		// ReSharper disable InconsistentNaming
		private const int TConnectionsShutdownPollTimeout = 10;
		private const int TConnectionsActivePollTimeout = 1000;

		private const int TNodeChangeShutdownPollTimeout = 10;
		private const int TNodeChangeActivePollTimeout = 1000;
		// ReSharper restore InconsistentNaming

		private ulong messageIdCounter = 1;
		

		// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
		private readonly Thread tConnections;
		private readonly ManualResetEvent tConnectionsActive = new ManualResetEvent(true);
		private readonly ManualResetEvent tConnectionsShutdown = new ManualResetEvent(false);
		
		private readonly Thread tNodeChange;
		private readonly ManualResetEvent tNodeChangeActive = new ManualResetEvent(false);
		private readonly ManualResetEvent tNodeChangeShutdown = new ManualResetEvent(false);
		private readonly ManualResetEvent tNodeChangeSignalRunning = new ManualResetEvent(false);
		private HashSet<Guid> tNodeChangeAccessibleNodes;
		private DateTime tNodeChangeFirstChange;
		private long tNodeChangeLastChangeTicks;
		
		
		
		private readonly TcpListener listener;

		private List<Connection> connections = new List<Connection>();

		public IEnumerable<Connection> Connections => this.connections.ToArray();

		private readonly LinkStateSequence linkStateSequence = new LinkStateSequence();
		private readonly LinkStateDatabase linkStateDatabase;

		private readonly List<MessageExpectancy> expectancies = new List<MessageExpectancy>();
		
		private readonly Timer heartbeatTimer;
		private readonly Timer linkStateTimer;

		#region Callbacks

		private readonly CallbackRegistry<Message, Connection> messageHandlers = new CallbackRegistry<Message, Connection>();
		private readonly CallbackRegistry<Exception, CommNode> exceptionHandlers = new CallbackRegistry<Exception, CommNode>();

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Lazy<CallbackRegistryWrapper<Message, Connection>> messageHandlersWrapper;
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Lazy<CallbackRegistryWrapper<Exception, CommNode>> exceptionHandlersWrapper;

		public ICallbackRegistry<Message, Connection> MessageHandlers => this.messageHandlersWrapper.Value;
		public ICallbackRegistry<Exception, CommNode> ExceptionHandlers => this.exceptionHandlersWrapper.Value;


		public event Callback<Message, Connection> OnAnyMessage
		{
			add => this.MessageHandlers.Subscribe(value);
			remove => this.MessageHandlers.Revoke(value);
		}
		public event Callback<Exception, CommNode> OnAnyException
		{
			add => this.ExceptionHandlers.Subscribe(value);
			remove => this.ExceptionHandlers.Revoke(value);
		}

		#endregion Callbacks

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int timeout = 100;
		public int Timeout
		{
			get => this.timeout;
			set
			{
				this.timeout = value;
				foreach (var connection in this.connections)
				{
					connection.SetTimeout(this.Timeout);
				}
			}
		}


		public int NodeChangeMinWaitTime { get; set; } = 50;
		public int NodeChangeMaxWaitTime { get; set; } = 1000;

		private static readonly int defaultHeartbeatTime = (int)new TimeSpan(0, 0, 0, 15, 0).TotalMilliseconds;
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int heartbeatTime = CommNode.defaultHeartbeatTime;
		public int HeartbeatTime
		{
			get => this.heartbeatTime;
			set
			{
				if (value <= 0)
					throw new ArgumentException(nameof(value));

				this.heartbeatTime = value;
				this.heartbeatTimer.Change(0, this.heartbeatTime);
			}
		}

		private static readonly int defaultLinkStateTime = (int)new TimeSpan(0, 0, 1, 0, 0).TotalMilliseconds;
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int linkStateTime = CommNode.defaultLinkStateTime;
		public int LinkStateTime
		{
			get => this.linkStateTime;
			set
			{
				if (value <= 0)
					throw new ArgumentException(nameof(value));

				this.linkStateTime = value;
				this.linkStateTimer.Change(0, this.linkStateTime);
			}
		}

		public Guid NodeId { get; } = Guid.NewGuid();

		public MessageRegistry Registry { get; }

		public RoutingTable RoutingTable => this.linkStateDatabase.Graph.RoutingTable;


		public event EventHandler<ConnectionCreatedEventArgs> ConnectionCreated;
		public event EventHandler<ConnectionEstablishedEventArgs> ConnectionEstablished;
		public event EventHandler<ConnectionClosedEventArgs> ConnectionClosed;

		public event EventHandler<NodesAddedEventArgs> NodesAdded;
		public event EventHandler<NodesRemovedEventArgs> NodesRemoved; 

		public IPEndPoint ListenEndPoint => (IPEndPoint) this.listener.LocalEndpoint;

		public CommNode(int listenPort, MessageRegistry registry = null, Guid? nodeId = null)
		: this(new IPEndPoint(IPAddress.Any, listenPort), registry, nodeId)
		{

		}

		public CommNode(IPEndPoint listenEndpoint, MessageRegistry registry = null, Guid? nodeId = null)
		{
			if (registry == null)
				registry = MessageRegistry.Default;

			if (nodeId.HasValue)
				this.NodeId = nodeId.Value;

			this.Registry = registry;

			this.linkStateDatabase = new LinkStateDatabase(this);

			this.listener = new TcpListener(listenEndpoint);
			this.listener.Start();


			this.tConnections = new Thread(this.TConnections_Run)
			{
				Name = "Comm Connections Thread",
				IsBackground = true
			};
			this.tConnections.Start();

			this.tNodeChange = new Thread(this.TNodeChange_Run)
			{
				Name = "Comm NodeChange Thread",
				IsBackground = true
			};
			this.tNodeChange.Start();

			this.heartbeatTimer = new Timer(_ => this.SendHeartbeat(), null, 0, this.HeartbeatTime);
			this.linkStateTimer = new Timer(_ => this.SendLinkStatePackage(), null, 0, this.LinkStateTime);

			messageHandlersWrapper = new Lazy<CallbackRegistryWrapper<Message, Connection>>(() => new CallbackRegistryWrapper<Message, Connection>(this.messageHandlers));
			exceptionHandlersWrapper = new Lazy<CallbackRegistryWrapper<Exception, CommNode>>(() => new CallbackRegistryWrapper<Exception, CommNode>(this.exceptionHandlers));

			this.MessageHandlers.Subscribe<LinkStatePackage>(OnLinkStatePackage);
		}
		
		public void Shutdown()
		{
			this.tConnectionsShutdown.Set();
			if (Thread.CurrentThread != this.tConnections)
				this.tConnections.Join();

			this.tNodeChangeShutdown.Set();
			if (Thread.CurrentThread != this.tNodeChange)
				this.tNodeChange.Join();

			this.listener.Stop();

			foreach (var connection in this.Connections)
			{
				connection.Shutdown();
			}
		}

		public string GenerateGraphviz()
		{
			return this.linkStateDatabase.Graph.GenerateGraphviz();
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return $"CommNode {this.NodeId.ToShortString()}";
		}

		internal void Try(Action action)
		{
#if DEBUG
			action();
#else
			try
			{
				action();
			}
			catch (Exception e)
			{
				this.ProcessException(e);
			}
#endif
		}

		internal T Try<T>(Func<T> action)
		{
#if DEBUG
			return action();
#else
			try
			{
				return action();
			}
			catch (Exception e)
			{
				this.ProcessException(e);
			}
#endif
		}

		[NotNull]
		public Connection ConnectTo(string hostNameOrAddress, int port)
		{
			var dnsEntry = Dns.GetHostEntry(hostNameOrAddress);

			var lastAddress = dnsEntry.AddressList.Last();
			foreach (var address in dnsEntry.AddressList)
			{
				try
				{
					return this.ConnectTo(address, port);
				}
				catch (SocketException e) when (e.SocketErrorCode == SocketError.ConnectionRefused)
				{
					if (lastAddress == address)
						throw;
				}
			}

			return null;
		}

		[NotNull]
		public Connection ConnectTo(IPAddress address, int port) => this.ConnectTo(new IPEndPoint(address, port));

		[NotNull]
		public Connection ConnectTo(IPEndPoint otherEndPoint)
		{
			var socket = new Socket(otherEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			socket.Connect(otherEndPoint);

			return this.AddConnection(socket);
		}

		public void Dispatch(Message message, RecipientsList recipients = null,
							 DispatchWaitPolicy waitPolicy = DispatchWaitPolicy.WaitForAll)
		{
			if (recipients == null)
				recipients = RecipientsList.Broadcast;

			if (recipients.Contains(this.NodeId))
				recipients.Remove(this.NodeId);

			var header = new MessageHeader
			{
				Origin = this.NodeId,
				Id = this.NextMessageId(),

			};
			recipients.WriteTo(header);

			message.Header = header;

			this.DispatchPackage(MessagePackage.Create(message, this.Registry), waitPolicy);
		}


		public void Respond(Message responseToMessage, Message message, bool directResponse = false) =>
			this.Respond(responseToMessage.Header, message, directResponse);

		public void Respond(MessageHeader responseToHeader, Message message, bool directResponse = false, DispatchWaitPolicy waitPolicy = DispatchWaitPolicy.WaitForAll)
		{
			RecipientsList recipients;
			if (directResponse)
				recipients = responseToHeader.Origin;
			else
			{
				recipients = RecipientsList.FromMessageHeader(responseToHeader);
				if (!recipients.IsBroadcast)
				{
					recipients.Add(responseToHeader.Origin);
					recipients.Remove(this.NodeId);
				}
			}

			var header = new MessageHeader
			{
				Origin = this.NodeId,
				Id = this.NextMessageId(),
				ResponseToOrigin = responseToHeader.Origin,
				ResponseToId = responseToHeader.Id,

			};
			recipients.WriteTo(header);

			message.Header = header;

			this.DispatchPackage(MessagePackage.Create(message, this.Registry), waitPolicy);
		}

		public ExpectancyRoot Expect()
		{
			return new ExpectancyRoot(this);
		}

		public void AddExpectancy(MessageExpectancy expectancy)
		{
			this.expectancies.Add(expectancy);
			this.Try(() => expectancy.Attach(this));
		}
		public void RemoveExpectancy(MessageExpectancy expectancy)
		{
			if (!this.expectancies.Contains(expectancy))
				return;
			this.Try(expectancy.Detach);
			this.expectancies.Remove(expectancy);
		}


		private ulong NextMessageId()
		{
			if (this.messageIdCounter == ulong.MaxValue)
				this.messageIdCounter = 1;
			else
				this.messageIdCounter++;
			return this.messageIdCounter;
		}


		private void DistributePackageToConnections(MessagePackage package, ISet<Guid> targets, Action<MessagePackageDispatchHandle> handleAction = null)
		{
			if (handleAction == null)
				handleAction = h => { };


			var gateways = this.RoutingTable.GetGatewaysFor(package).ToArraySafe();
			foreach (var gateway in gateways)
			{
				var conn = this.connections.FirstOrDefault(c => c.RemoteId == gateway.Gateway);
				if (conn == null)
					continue; // TODO: Add some fail handling. RoutingTable says there is a gateway, but Conn for gateway does not exist -> Error!

				var p = package.Clone();

				var gatewayTargetsOverlap = new HashSet<Guid>(targets);
				gatewayTargetsOverlap.IntersectWith(gateway.Remotes.Keys);
				targets.ExceptWith(gatewayTargetsOverlap);

				p.RelayTo.Clear();
				p.RelayTo.AddRange(gatewayTargetsOverlap.ToProtoGuids());

				p.Ttl--;

				var handle = conn.Dispatch(p);
				handleAction(handle);

				if (!targets.Any())
					break;

			}
		}

		internal void DispatchPackage(MessagePackage package, DispatchWaitPolicy waitPolicy)
		{
			var handles = new List<WaitHandle>();

			var recipients = RecipientsList.FromMessageHeader(package.Header);

			var targets = new HashSet<Guid>(recipients.IsBroadcast ? this.RoutingTable.AccessibleNodes : recipients);
			this.DistributePackageToConnections(package, targets, h => handles.Add(h.WaitHandle));

			if (handles.Count == 0)
				return;

			if (waitPolicy == DispatchWaitPolicy.WaitForAny)
				WaitHandle.WaitAny(handles.ToArray(), this.Timeout);
			else if (waitPolicy == DispatchWaitPolicy.WaitForAll)
				WaitHandle.WaitAll(handles.ToArray());
		}

		private void RelayPackage(MessagePackage package)
		{
			var targets = new HashSet<Guid>(package.RelayTo.ToGuids());
			targets.Remove(this.NodeId);
			if (!targets.Any())
				return;

			this.DistributePackageToConnections(package, targets);
		}
		internal void ProcessPackage(MessagePackage package, Connection sourceConnection)
		{
			this.RelayPackage(package);

			var recipients = RecipientsList.FromMessageHeader(package.Header);
			if (recipients.Contains(this.NodeId) || recipients.IsBroadcast)
			{
				if (package.IsKnownMessage)
				{
					this.ProcessAddressedMessage(package.Body, sourceConnection);
				}
				else
				{
					// TODO: What to do with unkown messages addressed to me?
				}
			}
		}

		private void ProcessAddressedMessage(Message message, Connection sourceConnection)
		{
			bool isExpected = false;
			var satisfiedExpectancies = new List<MessageExpectancy>();

			foreach (var expectancy in this.expectancies)
			{
				if (!expectancy.IsExpected(message))
					continue;
				expectancy.ProcessMessage(message);
				isExpected = true;
				if (expectancy.IsSatisfied())
					satisfiedExpectancies.Add(expectancy);
			}

			foreach (var filter in satisfiedExpectancies)
			{
				this.RemoveExpectancy(filter);
			}

			if (!isExpected)
				this.messageHandlers.Invoke(message, sourceConnection); // TODO: Add context for expected messages
		}

		internal void ProcessException(Exception e)
		{
			this.exceptionHandlers.Invoke(e, this);
		}
		
		internal void SignalConnectionEstablished(Connection connection)
		{
			var nodesBefore = new HashSet<Guid>(this.linkStateDatabase.Graph.NonUpdatingRoutingTable.AccessibleNodes);

			this.OnConnectionEstablished(new ConnectionEstablishedEventArgs(connection));

			this.SendLinkStatePackage();

			this.SignalNodeChange(nodesBefore);
			
		}

		private void SendHeartbeat()
		{
			return;

			var keepAlive = new Heartbeat();
			foreach (var connection in this.Connections)
			{
				if (connection.State != ConnectionState.Active && connection.State != ConnectionState.Maintenance)
					continue;

				var header = new MessageHeader
				{
					Origin = this.NodeId,
					Id = this.NextMessageId(),

				};
				connection.RemoteId.ToRecipientsList().WriteTo(header);

				connection.Dispatch(MessagePackage.Create(keepAlive, this.Registry, header)); //.WaitOnProcessed(this.Timeout);
			}
		}

		private void SendLinkStatePackage()
		{
			var lsp = new LinkStatePackage
			{
				Sequence = this.linkStateSequence.Next(),
				Age = 0,
			};

			lsp.Origin.FromGuid(this.NodeId);
			foreach (var connection in this.Connections)
			{
				if (connection.State != ConnectionState.Active && connection.State != ConnectionState.Maintenance)
					continue;

				var link = new LinkStatePackage.Link
				{
					Cost = 1
				};
				link.Neighbor.FromGuid(connection.RemoteId);
				lsp.Links.Add(link);
			}

			foreach (var connection in this.Connections)
			{
				var header = new MessageHeader
				{
					Origin = this.NodeId,
					Id = this.NextMessageId(),

				};
				connection.RemoteId.ToRecipientsList().WriteTo(header);

				connection.Dispatch(MessagePackage.Create(lsp, this.Registry, header)); //.WaitOnProcessed(this.Timeout);
			}
			this.linkStateDatabase.ForceUpdate(lsp);

		}

		private void OnLinkStatePackage(LinkStatePackage package, Connection sourceConnection)
		{
			var nodesBefore = new HashSet<Guid>(this.linkStateDatabase.Graph.NonUpdatingRoutingTable.AccessibleNodes);

			if (this.linkStateDatabase.Update(package))
			{
				foreach (var connection in this.Connections)
				{
					if (connection.RemoteId == sourceConnection.RemoteId)
						continue;

					package.Age++;
					var header = new MessageHeader
					{
						Origin = this.NodeId,
						Id = this.NextMessageId(),

					};
					connection.RemoteId.ToRecipientsList().WriteTo(header);

					connection.Dispatch(MessagePackage.Create(package, this.Registry, header)); //.WaitOnProcessed(this.Timeout);
				}

				this.SignalNodeChange(nodesBefore);
			}
			
		}
		
		private Connection AddConnection(Socket socket)
		{

			var connection = new Connection(this, socket);

			List<Connection> oldList, tmp;
			do
			{
				oldList = this.connections;
				var newList = new List<Connection>(oldList) {connection};

				tmp = Interlocked.CompareExchange(ref this.connections, newList, oldList);

			} while (tmp != oldList);

			connection.SetTimeout(this.Timeout);
			this.OnConnectionCreated(new ConnectionCreatedEventArgs(connection));
			return connection;
		}

		internal void RemoveConnection(Connection connection, ConnectionClosedCause cause)
		{
			var nodesBefore = new HashSet<Guid>(this.linkStateDatabase.Graph.NonUpdatingRoutingTable.AccessibleNodes);

			List<Connection> oldList, tmp;
			do
			{
				oldList = this.connections;
				var newList = new List<Connection>(oldList);
				newList.Remove(connection);

				tmp = Interlocked.CompareExchange(ref this.connections, newList, oldList);

			} while (tmp != oldList);

			if (this.tConnectionsShutdown.IsSet())
				return;
			this.OnConnectionClosed(new ConnectionClosedEventArgs(connection, cause));

			this.SendLinkStatePackage();
			this.SignalNodeChange(nodesBefore);
		}

		private void SignalNodeChange(HashSet<Guid> accessibleNodes)
		{
			if (this.tNodeChangeActive.WaitOne(0) || this.tNodeChangeSignalRunning.WaitOne(0))
			{

				var change = DateTime.Now;
				DateTime current;
				do
				{
					var oldTicks = this.tNodeChangeLastChangeTicks;
					var old = DateTime.FromBinary(oldTicks);
					if (old < change)
						break;

					var tmp = Interlocked.CompareExchange(ref this.tNodeChangeLastChangeTicks, change.ToBinary(), oldTicks);
					current = DateTime.FromBinary(tmp);
				} while (current < change);

				return;
			}

			this.tNodeChangeSignalRunning.Set();

			this.tNodeChangeAccessibleNodes = accessibleNodes;
			this.tNodeChangeFirstChange = DateTime.Now;

			this.tNodeChangeActive.Set();
			this.tNodeChangeSignalRunning.Reset();
		}
		private void TNodeChange_Run()
		{
			while (!this.tNodeChangeShutdown.WaitOne(CommNode.TNodeChangeShutdownPollTimeout))
			{
				if (!this.tNodeChangeActive.WaitOne(CommNode.TNodeChangeActivePollTimeout))
					continue; // If thread is suspended (tNodeChangeShutdown.WaitOne times out), restart while loop to make thread termination thru tNodeChangeShutdown possible

				var lastChange =
					DateTime.FromBinary(Interlocked.CompareExchange(ref this.tNodeChangeLastChangeTicks, 0, 0));

				var firstDiff = DateTime.Now - this.tNodeChangeFirstChange;
				var lastDiff = DateTime.Now - lastChange;

				if (this.NodeChangeMinWaitTime > 0 && lastDiff.TotalMilliseconds < this.NodeChangeMinWaitTime		// Not waited enough after last update ...
												   && (this.NodeChangeMaxWaitTime <= 0 || firstDiff.TotalMilliseconds < this.NodeChangeMaxWaitTime))	// ... and either no max wait time set, or not over it
					continue;
				
				this.tNodeChangeSignalRunning.Set();
				var preAccessibleNodes = this.tNodeChangeAccessibleNodes;
				

				var postAccessibleNodes = new HashSet<Guid>(this.RoutingTable.AccessibleNodes);


				this.tNodeChangeActive.Reset();
				this.tNodeChangeSignalRunning.Reset();

				var added = new HashSet<Guid>(postAccessibleNodes);
				added.ExceptWith(preAccessibleNodes);

				var removed = new HashSet<Guid>(preAccessibleNodes);
				removed.ExceptWith(postAccessibleNodes);
				
				if (removed.Count > 0)
					this.OnNodesRemoved(new NodesRemovedEventArgs(new ReadOnlyCollection<Guid>(removed.ToList())));
				if (added.Count > 0)
					this.OnNodesAdded(new NodesAddedEventArgs(new ReadOnlyCollection<Guid>(added.ToList())));
			}
		}

		private void TConnections_Run()
		{

			while (!this.tConnectionsShutdown.WaitOne(CommNode.TConnectionsShutdownPollTimeout))
			{
				if (!this.tConnectionsActive.WaitOne(CommNode.TConnectionsActivePollTimeout))
					continue; // If thread is suspended (tConnectionsActive.WaitOne times out), restart while loop to make thread termination thru tConnectionsShutdown possible

				foreach (var expectancy in this.expectancies)
				{
					this.Try(expectancy.CheckTimeout);
				}

				if (!this.listener.Pending())
					continue;

				var socket = this.listener.AcceptSocket();
				this.AddConnection(socket);

			}
		}

		protected virtual void OnConnectionCreated(ConnectionCreatedEventArgs e)
		{
			if (this.ConnectionCreated == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<ConnectionCreatedEventArgs> del in this.ConnectionCreated.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
		}

		protected virtual void OnConnectionEstablished(ConnectionEstablishedEventArgs e)
		{
			if (this.ConnectionEstablished == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<ConnectionEstablishedEventArgs> del in this.ConnectionEstablished.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
		}

		protected virtual void OnConnectionClosed(ConnectionClosedEventArgs e)
		{
			if (this.ConnectionClosed == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<ConnectionClosedEventArgs> del in this.ConnectionClosed.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
		}

		protected virtual void OnNodesAdded(NodesAddedEventArgs e)
		{
			if (this.NodesAdded == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<NodesAddedEventArgs> del in this.NodesAdded.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
			
		}

		protected virtual void OnNodesRemoved(NodesRemovedEventArgs e)
		{
			if (this.NodesRemoved == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<NodesRemovedEventArgs> del in this.NodesRemoved.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
		}
	}
}
