﻿// /*
//  * ExceptionExtension.cs
//  *
//  *  Created on: 09:01
//  *         Author: 
//  */

using System;
using System.Reflection;

namespace Shortbit.Utils
{
	public static class ExceptionExtension
	{
		public static void PreserveStackTrace(this Exception e)
		{
			typeof (Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic)
				.Invoke(e, null);
			/*	StreamingContext ctx = new StreamingContext(StreamingContextStates.CrossAppDomain);
			ObjectManager mgr = new ObjectManager(null, ctx);
			SerializationInfo si = new SerializationInfo(e.GetType(), new FormatterConverter());

			e.GetObjectData(si, ctx);
			mgr.RegisterObject(e, 1, si); // prepare for SetObjectData
			mgr.DoFixups(); // ObjectManager calls SetObjectData*/

			// voila, e is unmodified save for _remoteStackTraceString
		}

		public static void Rethrow(this Exception ex)
		{
			ex.PreserveStackTrace();
			typeof (Exception).GetMethod("PrepForRemoting",
				BindingFlags.NonPublic | BindingFlags.Instance)
				.Invoke(ex, new object[0]);
			throw ex;
		}
	}
}