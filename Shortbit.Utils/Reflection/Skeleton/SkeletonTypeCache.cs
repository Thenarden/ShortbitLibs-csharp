﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public sealed class SkeletonTypeCache : IReadOnlyDictionary<Type, SkeletonType>
	{
		private readonly Dictionary<Type, SkeletonType> values;

		public SkeletonTypeCache()
		{
			this.values = new Dictionary<Type, SkeletonType>();
		}

		public SkeletonTypeCache(SkeletonTypeCache existing)
		{
			this.values = new Dictionary<Type, SkeletonType>(existing.values);
		}


		public void Clear()
		{
			this.values.Clear();
		}

		internal void Add(SkeletonType type)
		{
			this.values[type.SourceType] = type;
		}

		/// <summary>Determines whether the read-only dictionary contains an element that has the specified key.</summary>
		/// <returns>true if the read-only dictionary contains an element that has the specified key; otherwise, false.</returns>
		/// <param name="key">The key to locate.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.</exception>
		public bool ContainsKey(Type key) => this.values.ContainsKey(key);

		/// <summary>Gets the value that is associated with the specified key.</summary>
		/// <returns>true if the object that implements the <see cref="T:System.Collections.Generic.IReadOnlyDictionary`2" /> interface contains an element that has the specified key; otherwise, false.</returns>
		/// <param name="key">The key to locate.</param>
		/// <param name="value">When this method returns, the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value" /> parameter. This parameter is passed uninitialized.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="key" /> is null.</exception>
		public bool TryGetValue(Type key, out SkeletonType value) => this.values.TryGetValue(key, out value);

		public SkeletonType this[Type type] => this.values[type];

		/// <summary>Gets an enumerable collection that contains the keys in the read-only dictionary. </summary>
		/// <returns>An enumerable collection that contains the keys in the read-only dictionary.</returns>
		public IEnumerable<Type> Keys => this.values.Keys;

		/// <summary>Gets an enumerable collection that contains the values in the read-only dictionary.</summary>
		/// <returns>An enumerable collection that contains the values in the read-only dictionary.</returns>
		public IEnumerable<SkeletonType> Values => this.values.Values;

		/// <summary>Returns an enumerator that iterates through the collection.</summary>
		/// <returns>An enumerator that can be used to iterate through the collection.</returns>
		public IEnumerator<KeyValuePair<Type, SkeletonType>> GetEnumerator() => this.values.GetEnumerator();

		/// <summary>Returns an enumerator that iterates through a collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <summary>Gets the number of elements in the collection.</summary>
		/// <returns>The number of elements in the collection. </returns>
		public int Count => this.values.Count;
	}
	#endif
}
