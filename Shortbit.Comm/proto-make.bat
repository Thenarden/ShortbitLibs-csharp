@echo off

cd %~dp0
cd ..

echo Updating Common submodule
git.exe submodule update --init --force --remote -- "submodules/common"

cd Shortbit.Comm

echo Compiling

cd %~dp0

echo ##################################################################################
echo #  Compiling message structures
echo #


protoc @.\protobuf\protoc_options_structures.txt
if %errorlevel% neq 0 exit /b %errorlevel%

echo ##################################################################################
echo #  Compiling transport classes
echo #


protoc @.\protobuf\protoc_options_transport.txt
if %errorlevel% neq 0 exit /b %errorlevel%


echo ##################################################################################
echo #  Compiling LSN messages
echo #

protoc @.\protobuf\protoc_options_lsn.txt
if %errorlevel% neq 0 exit /b %errorlevel%


echo ##################################################################################
echo #  Compiling CommLib messages
echo #

protoc @.\protobuf\protoc_options_messages.txt
if %errorlevel% neq 0 exit /b %errorlevel%

echo ##################################################################################
echo #  done
echo #