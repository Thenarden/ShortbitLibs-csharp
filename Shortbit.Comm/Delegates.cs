﻿// /*
//  * Delegates.cs
//  *
//  *  Created on: 15:58
//  *		 Author: 
//  */

using System;
using System.Net;
using Shortbit.Comm.Messages;
using Shortbit.Comm.Transport;

namespace Shortbit.Comm
{

	//public delegate void ConnectionEstablishedHandlerDelegate(CommClient client);

	//public delegate void ConnectionAbortedHandlerDelegate(CommClient client, ConnectionAbortedEventArgs args);


	public class ConnectionAbortedEventArgs
	{
		public ConnectionAbortedEventArgs(ConnectionClosedCause cause, IPEndPoint remoteEndpoint, IPEndPoint localEndPoint)
		{
			Cause = cause;
			RemoteEndPoint = remoteEndpoint;
			LocalEndPoint = localEndPoint;
		}

		public ConnectionClosedCause Cause { get; private set; }

		public IPEndPoint RemoteEndPoint { get; private set; }
		public IPEndPoint LocalEndPoint { get; private set; }
	}


	public class ConnectionStateChangedEventArgs
	{
		public ConnectionStateChangedEventArgs(Connection connection, ConnectionState state, ConnectionState previousState)
		{
			this.Connection = connection;
			this.State = state;
			this.PreviousState = previousState;
		}

		public Connection Connection { get; }

		public ConnectionState State { get; }
		public ConnectionState PreviousState { get; }
	}

}