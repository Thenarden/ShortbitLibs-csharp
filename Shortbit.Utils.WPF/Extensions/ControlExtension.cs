﻿using System;
using System.Windows.Controls;

namespace Shortbit.Utils.WPF
{
	public static class ControlExtension
	{
		public static void UIThread(this Control control, Action code)
		{
			//	if (control.Disposing || control.IsDisposed)
			//		return;

			if (!control.CheckAccess())
			{
				control.Dispatcher.BeginInvoke(code);
				return;
			}
			code.Invoke();
		}

		public static void UIThreadInvoke(this Control control, Action code)
		{
			//	if (control.Disposing || control.IsDisposed)
			//		return;

			if (!control.CheckAccess())
			{
				control.Dispatcher.Invoke(code);
				return;
			}
			code.Invoke();
		}

		public static T UIThreadInvoke<T>(this Control control, Func<T> code)
		{
			//	if (control.Disposing || control.IsDisposed)
			//		return;

			if (!control.CheckAccess())
			{
				#if NET40
				return (T)control.Dispatcher.Invoke(code);
				#else
				return control.Dispatcher.Invoke(code);
				#endif
			}
			return code.Invoke();
		}
	}
}
