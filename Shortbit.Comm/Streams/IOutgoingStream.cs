﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Comm.Streams
{
    public interface IOutgoingStream
    {
        void Write(byte[] buffer, int offset, int count);

        IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state);
        void EndWrite(IAsyncResult asyncResult);
        
        Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken);
    }
}
