﻿using System.IO;

namespace Shortbit.Utils
{
	public static class PathEx
	{
		public static bool Exists(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
				return false;
			
			return File.Exists(path) || Directory.Exists(path);
		}

		public static bool IsFile(string path) => File.Exists(path);

		public static bool IsDirectory(string path) => Directory.Exists(path);
	}
}
