﻿// /*
//  * C_Caesar.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;

namespace Shortbit.Data.Cryptography
{
    public class C_Caesar : C_Base
    {
        public C_Caesar()
        {
            GenerateNewKey();
        }

        public Byte[] Key { get; set; }

        public Byte[] Encrypt(Byte[] Data)
        {
            var LocData = new Byte[Data.Length];
            int k = 0;

            if (Key.Length < 1)
            {
                Key = new Byte[2];
                Key[0] = 0;
            }

            int Multi = 0;
            if (Key[0] < 128) // First Key Value is in the lower half of Byte Value Range
                Multi = 1; // Normal Operation is "Add"
            else
                Multi = -1; // Alternate Operation is "Subtract"

            for (int i = 0; i < LocData.Length; i++)
            {
                int LocByte = Data[i];
                LocByte += Key[k]*Multi;

                if (LocByte < 0)
                    LocByte += 256;
                if (LocByte >= 256)
                    LocByte -= 256;

                LocData[i] = (Byte) LocByte;

                k++;
                if (k == Key.Length)
                    k = 0;
            }
            return LocData;
        }

        public Byte[] Decrypt(Byte[] Data)
        {
            var LocData = new Byte[Data.Length];
            int k = 0;

            if (Key.Length < 1)
            {
                Key = new Byte[2];
                Key[0] = 0;
            }

            int Multi = 0;
            if (Key[0] < 128) // First Key Value is in the lower half of Byte Value Range
                Multi = -1; // Normal Operation is "Subtract"
            else
                Multi = 1; // Alternate Operation is "Add"

            for (int i = 0; i < LocData.Length; i++)
            {
                int LocByte = Data[i];
                LocByte += Key[k]*Multi;

                if (LocByte < 0)
                    LocByte += 256;
                if (LocByte >= 256)
                    LocByte -= 256;

                LocData[i] = (Byte) LocByte;

                k++;
                if (k == Key.Length)
                    k = 0;
            }
            return LocData;
        }

        public C_Base Clone()
        {
            C_Base r = new C_Caesar();
            r.Key = Key;
            return r;
        }

        public void GenerateNewKey()
        {
            var rnd = new Random();

            var r = new Byte[256]; // 2048 bit key
            for (int i = 0; i < r.Length; i++)
                r[i] = (Byte) rnd.Next(256);
            Key = r;
        }
    }
}