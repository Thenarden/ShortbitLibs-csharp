﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Comm
{
	public class NodesRemovedEventArgs
	{
		public NodesRemovedEventArgs(IReadOnlyCollection<Guid> nodes)
		{
			this.Nodes = nodes;
		}

		public IReadOnlyCollection<Guid> Nodes { get; }
	}
}
