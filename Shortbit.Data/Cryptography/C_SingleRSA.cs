﻿// /*
//  * C_SingleRSA.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Shortbit.Data.Cryptography
{
    public class C_SingleRSA : C_Base
    {
        private RSACryptoServiceProvider worker;

        public C_SingleRSA()
        {
            KeySize = findLargestKeySize();
            GenerateNewKey();
        }

        public bool EncryptOnly { get; private set; }

        private int modulusSize
        {
            get
            {
                RSAParameters param = worker.ExportParameters(false);
                return param.Modulus.Length;
            }
        }

        public int KeySize { get; private set; }

        public byte[] Key
        {
            get
            {
                RSAParameters param = worker.ExportParameters(false); // never show private keys, too

                var r = new Byte[param.Exponent.Length + param.Modulus.Length + 1];

                r[0] = (Byte) param.Exponent.Length;
                for (int i = 0; i < param.Exponent.Length; i++) // 3
                    r[i + 1] = param.Exponent[i];
                for (int i = 0; i < param.Modulus.Length; i++)
                    r[param.Exponent.Length + i + 1] = param.Modulus[i];

                return r;
            }
            set // Sets the (only public) Key 
            {
                if (value.Length <= 0)
                    return;
                var Exponent = new Byte[value[0]];
                var Modulus = new Byte[value.Length - Exponent.Length - 1];

                for (int i = 0; i < Exponent.Length; i++)
                    Exponent[i] = value[i + 1];
                for (int i = 0; i < Modulus.Length; i++)
                    Modulus[i] = value[Exponent.Length + i + 1];

                var param = new RSAParameters();
                param.Exponent = Exponent;
                param.Modulus = Modulus;
                worker.ImportParameters(param);

                EncryptOnly = true;
            }
        }


        public byte[] Encrypt(byte[] Data)
        {
            int packetSize = modulusSize - 11;

            List<List<Byte>> partedData = GetParts(new List<byte>(Data), packetSize);
            var r = new List<byte>();

            bool First = true;
            int bytesPerPart = 0;

            foreach (List<Byte> part in partedData)
            {
                Byte[] encry = worker.Encrypt(part.ToArray(), false);
                r.AddRange(encry);

                if (First)
                    bytesPerPart = encry.Length;
                First = false;
            }
            r.InsertRange(0, BitConverter.GetBytes(bytesPerPart));
            return r.ToArray();
        }

        public byte[] Decrypt(byte[] _Data)
        {
            if (EncryptOnly)
                throw new InvalidOperationException("Cannot decrypt with public key only.");

            var Data = new List<byte>(_Data);

            int bytesPerPart = BitConverter.ToInt32(Data.GetRange(0, 4).ToArray(), 0);
            Data.RemoveRange(0, 4);

            List<List<Byte>> partedData = GetParts(Data, bytesPerPart);
            var r = new List<byte>();

            foreach (List<Byte> part in partedData)
            {
                Byte[] encry = worker.Decrypt(part.ToArray(), false);
                r.AddRange(encry);
            }
            return r.ToArray();
        }

        public C_Base Clone()
        {
            var r = new C_SingleRSA();

            r.worker.ImportParameters(worker.ExportParameters(true));
            return r;
        }

        public void GenerateNewKey()
        {
            worker = new RSACryptoServiceProvider(KeySize);
            EncryptOnly = false;
        }


        private List<List<Byte>> GetParts(List<Byte> Data, int bytesPerPart)
        {
            var partedData = new List<List<byte>>();
            while (Data.Count > 0)
            {
                var part = new List<byte>();

                if (Data.Count <= bytesPerPart)
                {
                    part.AddRange(Data);
                    Data.Clear();
                }
                else
                {
                    var bytes = new Byte[bytesPerPart];
                    for (int i = 0; i < bytes.Length; i++)
                        bytes[i] = Data[i];
                    part.AddRange(bytes);
                    Data.RemoveRange(0, bytesPerPart);
                }
                partedData.Add(part);
            }
            return partedData;
        }

        private int findLargestKeySize()
        {
            foreach (KeySizes keySize in new RSACryptoServiceProvider(512).LegalKeySizes)
            {
                if ((keySize.MinSize < 2048) && (2048 < keySize.MaxSize))
                    return 2048;
            }

            return 512;
        }
    }
}