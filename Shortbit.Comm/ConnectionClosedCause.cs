﻿// /*
//  * AbortionCause.cs
//  *
//  *  Created on: 15:58
//  *         Author: 
//  */

namespace Shortbit.Comm
{
    public enum ConnectionClosedCause
    {
        RemoteClosed,
        LocalClosed,
        TimedOut,
    }
}