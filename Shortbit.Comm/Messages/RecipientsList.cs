﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Comm.Messages
{

	public class RecipientsList : ISet<Guid>
	{
		public static RecipientsList Broadcast => new RecipientsList();

		public bool IsBroadcast => this.Count == 0;

		private readonly HashSet<Guid> recipients = new HashSet<Guid>();


		public RecipientsList()
		{ }
		
		public RecipientsList(IEnumerable<Guid> source)
		{
			this.recipients.UnionWith(source);
		}

		public static implicit operator RecipientsList(Guid recipient)
		{
			return new RecipientsList { recipient };
		}
		public static implicit operator RecipientsList(ProtoGuid recipient)
		{
			return new RecipientsList { recipient };
		}
		public static implicit operator RecipientsList(MsgGuid recipient)
		{
			return new RecipientsList { recipient };
		}

		public static RecipientsList FromMessage(Message message) => RecipientsList.FromMessageHeader(message.Header);

		public static RecipientsList FromMessageHeader(MessageHeader header)
		{
			return new RecipientsList(header.Recipients.Select(id => (Guid)id));
		}

		public void WriteTo(MessageHeader header)
		{
			header.Recipients.Clear();
			header.Recipients.AddRange(this.Select(id => id.ToProtoGuid()));
		}


		/// <inheritdoc />
		public IEnumerator<Guid> GetEnumerator() => this.recipients.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		

		/// <inheritdoc />
		public bool Add(Guid item) => this.recipients.Add(item);

		/// <inheritdoc />
		public void UnionWith(IEnumerable<Guid> other) => this.recipients.UnionWith(other);

		/// <inheritdoc />
		public void IntersectWith(IEnumerable<Guid> other) => this.recipients.IntersectWith(other);

		/// <inheritdoc />
		public void ExceptWith(IEnumerable<Guid> other) => this.recipients.ExceptWith(other);

		/// <inheritdoc />
		public void SymmetricExceptWith(IEnumerable<Guid> other) => this.recipients.SymmetricExceptWith(other);

		/// <inheritdoc />
		public bool IsSubsetOf(IEnumerable<Guid> other) => this.recipients.IsSubsetOf(other);

		/// <inheritdoc />
		public bool IsSupersetOf(IEnumerable<Guid> other) => this.recipients.IsSupersetOf(other);

		/// <inheritdoc />
		public bool IsProperSupersetOf(IEnumerable<Guid> other) => this.recipients.IsProperSupersetOf(other);

		/// <inheritdoc />
		public bool IsProperSubsetOf(IEnumerable<Guid> other) => this.recipients.IsProperSubsetOf(other);

		/// <inheritdoc />
		public bool Overlaps(IEnumerable<Guid> other) => this.recipients.Overlaps(other);

		/// <inheritdoc />
		public bool SetEquals(IEnumerable<Guid> other) => this.recipients.SetEquals(other);

		/// <inheritdoc />
		void ICollection<Guid>.Add(Guid item) => this.Add(item);


		public void AddRange(IEnumerable<Guid> other) => this.UnionWith(other);

		/// <inheritdoc />
		public void Clear() => this.recipients.Clear();

		/// <inheritdoc />
		public bool Contains(Guid item) => this.recipients.Contains(item);

		/// <inheritdoc />
		public void CopyTo(Guid[] array, int arrayIndex) => this.recipients.CopyTo(array, arrayIndex);

		/// <inheritdoc />
		public bool Remove(Guid item) => this.recipients.Remove(item);

		/// <inheritdoc />
		public int Count => this.recipients.Count;

		/// <inheritdoc />
		public bool IsReadOnly => (this.recipients as ISet<Guid>).IsReadOnly;
	}
}
