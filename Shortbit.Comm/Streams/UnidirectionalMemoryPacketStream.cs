﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Utils.IO;

namespace Shortbit.Comm.Streams
{
    public class UnidirectionalMemoryPacketStream
    {
        private class IncommingWrapperStream : IIncomingStatelessPacketStream
        {
            private readonly UnidirectionalMemoryPacketStream host;


            public bool DataAvailable => host.stream.Length > 0;

            public IStreamEndpoint LocalEndpoint { get; }
            public IStreamEndpoint RemoteEndpoint { get; }

            public IncommingWrapperStream(UnidirectionalMemoryPacketStream host)
            {
                this.host = host;
                this.LocalEndpoint = new MemoryStreamEndpoint();
                this.RemoteEndpoint = new MemoryStreamEndpoint();
            }

            public int Read(byte[] buffer, int offset, int count)
            {
                return host.stream.Read(buffer, offset, count);
            }

            public IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
            {
                return host.stream.BeginRead(buffer, offset, count, callback, state);
            }

            public int EndRead(IAsyncResult asyncResult)
            {
                return host.stream.EndRead(asyncResult);
            }

            public Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
            {
                return host.stream.ReadAsync(buffer, offset, count, cancellationToken);
            }
        }

        private class OutgoingStreamWrapper : IOutgoingStatelessPacketStream
        {
            private readonly UnidirectionalMemoryPacketStream host;

            public IStreamEndpoint LocalEndpoint { get; }
            public IStreamEndpoint RemoteEndpoint { get; }

            public OutgoingStreamWrapper(UnidirectionalMemoryPacketStream host)
            {
                this.host = host;
                this.LocalEndpoint = new MemoryStreamEndpoint();
                this.RemoteEndpoint = new MemoryStreamEndpoint();
            }


            public void Write(byte[] buffer, int offset, int count)
            {
                host.stream.Write(buffer, offset, count);
            }

            public IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
            {
                return host.stream.BeginWrite(buffer, offset, count, callback, state);
            }

            public void EndWrite(IAsyncResult asyncResult)
            {
                host.stream.EndWrite(asyncResult);
            }

            public Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
            {
                return host.stream.WriteAsync(buffer, offset, count, cancellationToken);
            }
        }
            
        private readonly PipeStream stream;

        public IIncomingStatelessPacketStream IncomingStream { get; }
        public IOutgoingStatelessPacketStream OutgoingStream { get; }

        public UnidirectionalMemoryPacketStream()
            : this(new PipeStream())
        { }
        public UnidirectionalMemoryPacketStream(PipeStream stream)
        {
            this.stream = stream;
            this.IncomingStream = new IncommingWrapperStream(this);
            this.OutgoingStream = new OutgoingStreamWrapper(this);
        }

        

    }
}
