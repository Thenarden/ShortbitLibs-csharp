﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public sealed class SkeletonTypeComplex : SkeletonType
	{

		public static bool IsComplexType(Type type)
		{
		//	return (type.IsClass && type != typeof(string)) || 
		//		   (type.IsValueType && !type.IsPrimitive && !type.IsEnum && type != typeof(DateTime) && type != typeof(TimeSpan));
			return !SkeletonTypePrimitive.IsPrimitiveType(type) && !SkeletonTypeEnum.IsEnumType(type) &&
				   !SkeletonTypeCollection.IsCollectionType(type) && !SkeletonTypeDictionary.IsDictionaryType(type);
		}

		public static SkeletonTypeComplex CreateComplex(Type sourceType) =>
			SkeletonTypeComplex.CreateComplex(new SkeletonTypeCreationContext(), sourceType, true);

		public static SkeletonTypeComplex CreateComplex(SkeletonTypeCreationContext context, Type sourceType) =>
			SkeletonTypeComplex.CreateComplex(context, sourceType, true);

		internal static SkeletonTypeComplex CreateComplex(SkeletonTypeCreationContext context, Type sourceType, bool typeCheck)
		{
			if (typeCheck && !SkeletonTypeComplex.IsComplexType(sourceType))
				throw new ArgumentException($"Given type \"{sourceType.FullName}\" is not a complex type.");

			if (context.Cache.ContainsKey(sourceType))
				return (SkeletonTypeComplex) context.Cache[sourceType];
			
			var properties = new Dictionary<string, SkeletonProperty>();
			var fields = new Dictionary<string, SkeletonField>();

			foreach (var info in sourceType.GetProperties(
				BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (!context.PropertyFilter(sourceType, info))
					continue;

				var p = SkeletonProperty.CreateProperty(context, info);
				if (!properties.ContainsKey(p.Name) || (properties[p.Name].PropertyInfo.DeclaringType?.IsAssignableFrom(info.DeclaringType) ?? true)) 
					properties[p.Name] = p; // Either not defined, or defined in superclass (alias overwritten by "new")
			}
			
			foreach (var info in sourceType.GetFields(
				BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (!context.FieldFilter(sourceType, info))
					continue;

				var p = SkeletonField.CreateField(context, info);
				if (!fields.ContainsKey(p.Name) || (fields[p.Name].FieldInfo.DeclaringType?.IsAssignableFrom(info.DeclaringType) ?? true)) 
					fields[p.Name] = p; // Either not defined, or defined in superclass (alias overwritten by "new")
			}

			var res = new SkeletonTypeComplex(sourceType, properties.AsReadOnly(), fields.AsReadOnly());
			context.Cache.Add(res);
			return res;
		}


		private SkeletonTypeComplex(Type sourceType, IReadOnlyDictionary<string, SkeletonProperty> properties, IReadOnlyDictionary<string, SkeletonField> fields) : base(sourceType)
		{
			this.Properties = properties;
			this.Fields = fields;

			foreach (var property in this.Properties.Values)
				property.DeclaringType = this;
			foreach (var field in this.Fields.Values)
				field.DeclaringType = this;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString() => $"Complex <{this.FullName}>";
		
		public IReadOnlyDictionary<string, SkeletonProperty> Properties { get; }
		public IReadOnlyDictionary<string, SkeletonField> Fields { get; }
		


		public abstract class MemberInfoBase 
		{
			public string Name { get; }
			public SkeletonType Type { get; }

			[NotNull]
			public SkeletonTypeComplex DeclaringType { get; internal set; }
			

			protected MemberInfoBase(string name, SkeletonType type)
			{
				this.Name = name;
				this.Type = type;
			}

		}

		public class SkeletonProperty : MemberInfoBase
		{
			public PropertyInfo PropertyInfo { get; }

			public bool CanRead { get; }
			public bool CanWrite { get; }

			internal static SkeletonProperty CreateProperty(SkeletonTypeCreationContext context, PropertyInfo info)
			{
				var type = SkeletonType.Create(context, info.PropertyType);
				return new SkeletonProperty(info, info.Name, type, info.GetMethod.IsPublic, info.SetMethod.IsPublic);
			}
			
			private SkeletonProperty(PropertyInfo propertyInfo, string name, SkeletonType type, bool canRead, bool canWrite)  : base(name, type)
			{
				this.PropertyInfo = propertyInfo;
				this.CanRead = canRead;
				this.CanWrite = canWrite;
				this.PropertyInfo = propertyInfo;
			}

		}

		public class SkeletonField : MemberInfoBase
		{
			public FieldInfo FieldInfo { get; }
			public bool IsPublic { get; }
			public bool IsPrivate { get; }
			
			internal static SkeletonField CreateField(SkeletonTypeCreationContext context, FieldInfo info)
			{
				var type = SkeletonType.Create(context, info.FieldType);
				return new SkeletonField(info, info.Name, type, info.IsPublic, info.IsPrivate);
			}

			private SkeletonField(FieldInfo fieldInfo, string name, SkeletonType type, bool isPublic, bool isPrivate) : base(name, type)
			{
				this.FieldInfo = fieldInfo;
				this.IsPublic = isPublic;
				this.IsPrivate = isPrivate;
			}
		}
	}
	#endif
}
