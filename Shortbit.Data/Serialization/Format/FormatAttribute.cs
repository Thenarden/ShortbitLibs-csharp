﻿using System;

namespace Shortbit.Data.Serialization
{
    public class FormatAttribute : Attribute
    {
        public Type StoreType { get; set; }
        public Type Formatter { get; set; }

        public FormatAttribute(Type storeType, Type formatter)
        {
            this.StoreType = storeType;
            this.Formatter = formatter;
        }
    }
}
