﻿// /*
//  * IShift.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IShift<T> : IOperator<T>
	{
		T ShiftLeft(ref T value, int amount);

		void AssignShiftLeft(ref T value, int amount);

		T ShiftRight(ref T value, int amount);

		void AssignShiftRight(ref T value, int amount);
	}
}