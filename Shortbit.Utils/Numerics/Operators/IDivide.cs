﻿// /*
//  * IDivide.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IDivide<T> : IOperator<T>
	{
		T Divide(ref T a, ref T b);


		void AssignDivide(ref T first, ref T other);
	}
}