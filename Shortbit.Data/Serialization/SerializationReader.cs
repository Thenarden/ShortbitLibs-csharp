﻿// /*
//  * SerializationReader.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System.IO;
using System.Text;

namespace Shortbit.Data.Serialization
{
    public sealed class SerializationReader
    {
        private readonly BinaryReader reader;

        public SerializationReader(Stream input)
        {
            reader = new BinaryReader(input, Encoding.UTF8);
        }


        //
        // Zusammenfassung:
        //     Gibt das nächste verfügbare Zeichen zurück, ohne die Byte- oder Zeichenposition
        //     zu erhöhen.
        //
        // Rückgabewerte:
        //     Das nächste verfügbare Zeichen oder -1, wenn keine weiteren Zeichen verfügbar
        //     sind oder der Stream keine Suchvorgänge unterstützt.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public int PeekChar()
        {
            return reader.PeekChar();
        }

        //
        // Zusammenfassung:
        //     Liest Zeichen aus dem zugrunde liegenden Stream und erhöht die aktuelle Position
        //     im Stream in Abhängigkeit von der verwendeten Encoding und dem aus dem Stream
        //     gelesenen Zeichen.
        //
        // Rückgabewerte:
        //     Das nächste Zeichen aus dem Eingabestream bzw. -1, wenn derzeit keine weiteren
        //     Zeichen verfügbar sind.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public int Read()
        {
            return reader.Read();
        }

        //
        // Zusammenfassung:
        //     Liest count Bytes aus dem Stream, wobei index als Anfangspunkt im Bytearray
        //     verwendet wird.
        //
        // Parameter:
        //   buffer:
        //     Der Puffer, in den Daten gelesen werden sollen.
        //
        //   index:
        //     Der Anfangspunkt für das Lesen in den Puffer.
        //
        //   count:
        //     Die Anzahl der zu lesenden Zeichen.
        //
        // Rückgabewerte:
        //     Die Anzahl der in den buffer gelesenen Zeichen. Diese kann kleiner sein als
        //     die Anzahl der angeforderten Bytes, wenn gegenwärtig keine entsprechende
        //     Anzahl von Bytes verfügbar ist, oder null, wenn das Ende des Streams erreicht
        //     ist.
        //
        // Ausnahmen:
        //   System.ArgumentException:
        //     Die Länge des Puffers minus index ist kleiner als count.
        //
        //   System.ArgumentNullException:
        //     buffer ist null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index oder count ist negativ.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public int Read(byte[] buffer, int index, int count)
        {
            return reader.Read(buffer, index, count);
        }

        //
        // Zusammenfassung:
        //     Liest count Zeichen aus dem Stream, wobei index als Anfangspunkt im Zeichenarray
        //     verwendet wird.
        //
        // Parameter:
        //   buffer:
        //     Der Puffer, in den Daten gelesen werden sollen.
        //
        //   index:
        //     Der Anfangspunkt für das Lesen in den Puffer.
        //
        //   count:
        //     Die Anzahl der zu lesenden Zeichen.
        //
        // Rückgabewerte:
        //     Die Gesamtanzahl der in den Puffer gelesenen Zeichen. Diese kann kleiner
        //     sein als die Anzahl der angeforderten Zeichen, wenn gegenwärtig nicht so
        //     viele Zeichen verfügbar sind, oder null, wenn das Ende des Streams erreicht
        //     ist.
        //
        // Ausnahmen:
        //   System.ArgumentException:
        //     Die Länge des Puffers minus index ist kleiner als count.
        //
        //   System.ArgumentNullException:
        //     buffer ist null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index oder count ist negativ.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public int Read(char[] buffer, int index, int count)
        {
            return reader.Read(buffer, index, count);
        }

        //
        // Zusammenfassung:
        //     Liest einen Boolean-Wert aus dem aktuellen Stream und erhöht die aktuelle
        //     Position im Stream um 1 Byte.
        //
        // Rückgabewerte:
        //     true wenn das Byte ungleich 0 (null) ist, andernfalls false.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public bool ReadBoolean()
        {
            return reader.ReadBoolean();
        }

        //
        // Zusammenfassung:
        //     Liest das nächste Byte aus dem aktuellen Stream und erhöht die aktuelle Position
        //     im Stream um 1 Byte.
        //
        // Rückgabewerte:
        //     Das nächste aus dem aktuellen Stream gelesene Byte.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public byte ReadByte()
        {
            return reader.ReadByte();
        }

        //
        // Zusammenfassung:
        //     Liest count Bytes aus dem aktuellen Stream in ein Bytearray und erhöht die
        //     aktuelle Position im Stream um count Byte(s).
        //
        // Parameter:
        //   count:
        //     Die Anzahl der zu lesenden Bytes.
        //
        // Rückgabewerte:
        //     Ein Bytearray mit Daten aus dem zugrunde liegenden Stream. Dies kann kleiner
        //     sein als die Anzahl der angeforderten Bytes, wenn das Ende des Streams erreicht
        //     ist.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.ArgumentOutOfRangeException:
        //     count ist negativ.
        public byte[] ReadBytes(int count)
        {
            return reader.ReadBytes(count);
        }

        //
        // Zusammenfassung:
        //     Liest das nächste Zeichen aus dem aktuellen Stream und erhöht die aktuelle
        //     Position im Stream in Abhängigkeit von der verwendeten Encoding und dem aus
        //     dem Stream gelesenen Zeichen.
        //
        // Rückgabewerte:
        //     Ein aus dem aktuellen Stream gelesenes Zeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ArgumentException:
        //     Ein Ersatzzeichenzeichen wurde gelesen.
        public char ReadChar()
        {
            return reader.ReadChar();
        }

        //
        // Zusammenfassung:
        //     Liest count Zeichen aus dem aktuellen Stream, gibt die Daten in einem Zeichenarray
        //     zurück und erhöht die aktuelle Position in Abhängigkeit von der verwendeten
        //     Encoding und dem aus dem Stream gelesenen Zeichen.
        //
        // Parameter:
        //   count:
        //     Die Anzahl der zu lesenden Zeichen.
        //
        // Rückgabewerte:
        //     Ein Zeichenarray mit Daten aus dem zugrunde liegenden Stream. Dies kann kleiner
        //     sein als die Anzahl der angeforderten Zeichen, wenn das Ende des Streams
        //     erreicht ist.
        //
        // Ausnahmen:
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ArgumentOutOfRangeException:
        //     count ist negativ.
        public char[] ReadChars(int count)
        {
            return reader.ReadChars(count);
        }

        //
        // Zusammenfassung:
        //     Liest einen Dezimalwert aus dem aktuellen Stream und erhöht die aktuelle
        //     Position im Stream um 16 Bytes.
        //
        // Rückgabewerte:
        //     Ein aus dem aktuellen Stream gelesener Dezimalwert.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public decimal ReadDecimal()
        {
            return reader.ReadDecimal();
        }

        //
        // Zusammenfassung:
        //     Liest einen 8-Byte-Gleitkommawert aus dem aktuellen Stream und erhöht die
        //     aktuelle Position im Stream um 8 Bytes.
        //
        // Rückgabewerte:
        //     Ein aus dem aktuellen Stream gelesener 8-Byte-Gleitkommawert.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public double ReadDouble()
        {
            return reader.ReadDouble();
        }

        //
        // Zusammenfassung:
        //     Liest eine 2-Byte-Ganzzahl mit Vorzeichen aus dem aktuellen Stream und erhöht
        //     die aktuelle Position im Stream um 2 Bytes.
        //
        // Rückgabewerte:
        //     Eine aus dem aktuellen Stream gelesene 2-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public short ReadInt16()
        {
            return reader.ReadInt16();
        }

        //
        // Zusammenfassung:
        //     Liest eine 4-Byte-Ganzzahl mit Vorzeichen aus dem aktuellen Stream und erhöht
        //     die aktuelle Position im Stream um 4 Bytes.
        //
        // Rückgabewerte:
        //     Eine aus dem aktuellen Stream gelesene 4-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public int ReadInt32()
        {
            return reader.ReadInt32();
        }

        //
        // Zusammenfassung:
        //     Liest eine 8-Byte-Ganzzahl mit Vorzeichen aus dem aktuellen Stream und erhöht
        //     die aktuelle Position im Stream um 8 Bytes.
        //
        // Rückgabewerte:
        //     Eine aus dem aktuellen Stream gelesene 8-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public long ReadInt64()
        {
            return reader.ReadInt64();
        }

        //
        // Zusammenfassung:
        //     Liest ein Byte mit Vorzeichen aus dem aktuellen Stream und erhöht die aktuelle
        //     Position im Stream um ein Byte.
        //
        // Rückgabewerte:
        //     Ein aus dem aktuellen Stream gelesenes Byte mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public sbyte ReadSByte()
        {
            return reader.ReadSByte();
        }

        //
        // Zusammenfassung:
        //     Liest einen 4-Byte-Gleitkommawert aus dem aktuellen Stream und erhöht die
        //     aktuelle Position im Stream um 4 Bytes.
        //
        // Rückgabewerte:
        //     Ein aus dem aktuellen Stream gelesener 4-Byte-Gleitkommawert.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public float ReadSingle()
        {
            return reader.ReadSingle();
        }

        //
        // Zusammenfassung:
        //     Liest eine Zeichenfolge aus dem aktuellen Stream. Die Zeichenfolge weist
        //     ein Präfix mit der Länge auf, die als Ganzzahl mit jeweils 7 Bits codiert
        //     ist.
        //
        // Rückgabewerte:
        //     Die gelesene Zeichenfolge.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public string ReadString()
        {
            return reader.ReadString();
        }

        //
        // Zusammenfassung:
        //     Liest eine 2-Byte-Ganzzahl ohne Vorzeichen mithilfe einer Little-Endian-Codierung
        //     aus dem aktuellen Stream und erhöht die aktuelle Position im Stream um 2
        //     Bytes.
        //
        // Rückgabewerte:
        //     Eine aus diesem Stream gelesene 2-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public ushort ReadUInt16()
        {
            return reader.ReadUInt16();
        }

        //
        // Zusammenfassung:
        //     Liest eine 4-Byte-Ganzzahl ohne Vorzeichen aus dem aktuellen Stream und erhöht
        //     die aktuelle Position im Stream um 4 Bytes.
        //
        // Rückgabewerte:
        //     Eine aus diesem Stream gelesene 4-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public uint ReadUInt32()
        {
            return reader.ReadUInt32();
        }

        //
        // Zusammenfassung:
        //     Liest eine 8-Byte-Ganzzahl ohne Vorzeichen aus dem aktuellen Stream und erhöht
        //     die aktuelle Position im Stream um 8 Bytes.
        //
        // Rückgabewerte:
        //     Eine aus diesem Stream gelesene 8-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.EndOfStreamException:
        //     Das Ende des Streams ist erreicht.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public ulong ReadUInt64()
        {
            return reader.ReadUInt64();
        }
    }
}