﻿namespace Shortbit.Comm.Streams
{
    public interface IBidirectionalStatefullPacketStream : IBidirectionalStream, IStatefullPacketStream
    {
    }
}
