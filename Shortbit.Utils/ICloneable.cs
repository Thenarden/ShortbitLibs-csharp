﻿// /*
//  * ICloneable.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */


namespace Shortbit.Utils
{
	public interface ICloneable<T>
		where T : ICloneable<T>
	{
		T Clone();
	}
}