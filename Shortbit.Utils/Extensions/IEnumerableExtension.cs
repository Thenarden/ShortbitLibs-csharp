﻿// /*
//  * IEnumerableExtension.cs
//  *
//  *  Created on: 06:03
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shortbit.Utils.Collections.Generic;
using System.Threading.Tasks;

namespace Shortbit.Utils
{
	public static class IEnumerableExtension
	{
		/// <summary>
		///     Casts ot transforms the given enumerable to a list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="me">The IEnumerable to convert.</param>
		/// <returns>A list containing the same element as the input IEnumerable.</returns>
		public static List<T> ToListSafe<T>(this IEnumerable<T> me)
		{
			return me as List<T> ?? me.ToList();
		}

		/// <summary>
		///     Casts ot transforms the given enumerable to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="me">The IEnumerable to convert.</param>
		/// <returns>An array containing the same element as the input IEnumerable.</returns>
		public static T[] ToArraySafe<T>(this IEnumerable<T> me)
		{
			return me as T[] ?? me.ToArray();
		}

        /// <summary>
        /// Creates the symmetric difference between the two enumerables 
        /// containing the shared elements as well as the elements only in this 
        /// and the element only in the other enumerable.
        /// </summary>
        /// <typeparam name="T">The type of values to enumerate.</typeparam>
        /// <typeparam name="TV">The projection value to use for comparison.</typeparam>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <param name="projection"></param>
        /// <param name="equalityComparer"></param>
        /// <returns></returns>
	    public static CollectionDifference<T> DifferenceTo<T, TV>(this IEnumerable<T> me, IEnumerable<T> other, Func<T, TV> projection, IEqualityComparer<TV> equalityComparer )
	    {
            
	        var selfList = me.ToListSafe();
	        var otherList = other.ToListSafe();

	        var sharedKeys = selfList.Select(projection).Intersect(otherList.Select(projection), equalityComparer).ToListSafe();
            var onlyInSelf = selfList.Except(otherList, ProjectionEqualityComparer.Create(projection, equalityComparer));
	        var onlyInOther = otherList.Except(selfList, ProjectionEqualityComparer.Create(projection, equalityComparer));

            var shared = new List<Tuple<T, T>>(sharedKeys.Count);
	        if (sharedKeys.Any())
	        {
	            var selfDict = selfList.ToDictionary(projection, e => e);
	            var otherDict = otherList.ToDictionary(projection, e => e);

	            foreach (var k in sharedKeys)
	            {
	                shared.Add(Tuple.Create(selfDict[k], otherDict[k]));
	            }
	        }

	        return new CollectionDifference<T>
	        {
	            Shared = shared,
	            OnlyInSelf = onlyInSelf,
	            OnlyInOther = onlyInOther
	        };
        }
        /// <summary>
        /// Creates the symmetric difference between the two enumerables 
        /// containing the shared elements as well as the elements only in this 
        /// and the element only in the other enumerable.
        /// </summary>
        /// <typeparam name="T">The type of values to enumerate.</typeparam>
        /// <typeparam name="TV">The projection value to use for comparison.</typeparam>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <param name="projection"></param>
        /// <returns></returns>
	    public static CollectionDifference<T> DifferenceTo<T, TV>(this IEnumerable<T> me, IEnumerable<T> other, Func<T, TV> projection)
        {
            return me.DifferenceTo(other, projection, EqualityComparer<TV>.Default);
        }
        /// <summary>
        /// Creates the symmetric difference between the two enumerables 
        /// containing the shared elements as well as the elements only in this 
        /// and the element only in the other enumerable.
        /// </summary>
        /// <typeparam name="T">The type of values to enumerate.</typeparam>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <param name="equalityComparer"></param>
        /// <returns></returns>
	    public static CollectionDifference<T> DifferenceTo<T>(this IEnumerable<T> me, IEnumerable<T> other, IEqualityComparer<T> equalityComparer)
        {
            return me.DifferenceTo(other, e => e, equalityComparer);
        }
        /// <summary>
        /// Creates the symmetric difference between the two enumerables 
        /// containing the shared elements as well as the elements only in this 
        /// and the element only in the other enumerable.
        /// </summary>
        /// <typeparam name="T">The type of values to enumerate.</typeparam>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
	    public static CollectionDifference<T> DifferenceTo<T>(this IEnumerable<T> me, IEnumerable<T> other)
        {
            return me.DifferenceTo(other, e => e, EqualityComparer<T>.Default);
        }


        public static string ToListString(this IEnumerable me)
		{
			return "{" + string.Join(", ", me.Cast<object>().Select(x =>
			{
				if (x is string)
					return (x as string);
				if (x is IEnumerable)
					return (x as IEnumerable).ToListString();
				return x.ToString();
			})) + "}";
		}

		public static void DebugListString(this IEnumerable me)
		{
			DebugListStringWorker(me, "");
		}

		private static void DebugListStringWorker(IEnumerable me, string padding)
		{
			Debug.WriteLine("{");
			foreach (var obj in me)
			{
				if (obj is string)
					Debug.WriteLine(obj as string);
				else if (obj is IEnumerable)
					DebugListStringWorker(obj as IEnumerable, padding + "  ");
				else
					Debug.WriteLine(obj.ToString());
			}

			Debug.WriteLine("}");
		}

		public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> me, int N)
		{
			var e = me.ToListSafe();
			return e.Skip(Math.Max(0, e.Count - N));
		}

		public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> me, int N)
		{
			var e = me.ToListSafe();
			return e.Take(Math.Max(0, e.Count - N));
		}

		
#if NETCOREAPP
		public static IAsyncEnumerable<T> WrapAsync<T>(this IEnumerable<T> self)
		{
			return new WrappedAsyncEnumerable<T>(self);
		}
#endif

		#region Zip overloadings

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			Func<TFirst, TSecond, TThird, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TFouth, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			Func<TFirst, TSecond, TThird, TFouth, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TFouth, TFifth, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			IEnumerable<TFifth> fifth,
			Func<TFirst, TSecond, TThird, TFouth, TFifth, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			using (var enum5 = fifth.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext()
				       && enum5.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current,
						enum5.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			IEnumerable<TFifth> fifth,
			IEnumerable<TSixth> sixth,
			Func<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			using (var enum5 = fifth.GetEnumerator())
			using (var enum6 = sixth.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext()
				       && enum5.MoveNext()
				       && enum6.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current,
						enum5.Current,
						enum6.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			IEnumerable<TFifth> fifth,
			IEnumerable<TSixth> sixth,
			IEnumerable<TSeventh> seventh,
			Func<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			using (var enum5 = fifth.GetEnumerator())
			using (var enum6 = sixth.GetEnumerator())
			using (var enum7 = seventh.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext()
				       && enum5.MoveNext()
				       && enum6.MoveNext()
				       && enum7.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current,
						enum5.Current,
						enum6.Current,
						enum7.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TEighth, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			IEnumerable<TFifth> fifth,
			IEnumerable<TSixth> sixth,
			IEnumerable<TSeventh> seventh,
			IEnumerable<TEighth> eighth,
			Func<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TEighth, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			using (var enum5 = fifth.GetEnumerator())
			using (var enum6 = sixth.GetEnumerator())
			using (var enum7 = seventh.GetEnumerator())
			using (var enum8 = eighth.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext()
				       && enum5.MoveNext()
				       && enum6.MoveNext()
				       && enum7.MoveNext()
				       && enum8.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current,
						enum5.Current,
						enum6.Current,
						enum7.Current,
						enum8.Current);
				}
			}
		}

		public static IEnumerable<TResult> Zip
			<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TEighth, TNinth, TResult>(
			this IEnumerable<TFirst> first,
			IEnumerable<TSecond> second,
			IEnumerable<TThird> third,
			IEnumerable<TFouth> fourth,
			IEnumerable<TFifth> fifth,
			IEnumerable<TSixth> sixth,
			IEnumerable<TSeventh> seventh,
			IEnumerable<TEighth> eighth,
			IEnumerable<TNinth> ninth,
			Func<TFirst, TSecond, TThird, TFouth, TFifth, TSixth, TSeventh, TEighth, TNinth, TResult> resultSelector)
		{
			using (var enum1 = first.GetEnumerator())
			using (var enum2 = second.GetEnumerator())
			using (var enum3 = third.GetEnumerator())
			using (var enum4 = fourth.GetEnumerator())
			using (var enum5 = fifth.GetEnumerator())
			using (var enum6 = sixth.GetEnumerator())
			using (var enum7 = seventh.GetEnumerator())
			using (var enum8 = eighth.GetEnumerator())
			using (var enum9 = ninth.GetEnumerator())
			{
				while (enum1.MoveNext()
				       && enum2.MoveNext()
				       && enum3.MoveNext()
				       && enum4.MoveNext()
				       && enum5.MoveNext()
				       && enum6.MoveNext()
				       && enum7.MoveNext()
				       && enum8.MoveNext()
				       && enum9.MoveNext())
				{
					yield return resultSelector(
						enum1.Current,
						enum2.Current,
						enum3.Current,
						enum4.Current,
						enum5.Current,
						enum6.Current,
						enum7.Current,
						enum8.Current,
						enum9.Current);
				}
			}
		}

		#endregion
	}
}