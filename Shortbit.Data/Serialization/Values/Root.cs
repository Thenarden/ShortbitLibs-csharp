﻿using Shortbit.Data.Serialization.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization.Values
{
    public abstract class Root<TS> : IValue
    {
        public Type Type { get; set; }

        public abstract void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<object> setter);
        public abstract void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter);
    }
}
