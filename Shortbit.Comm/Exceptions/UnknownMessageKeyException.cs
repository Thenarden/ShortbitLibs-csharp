﻿// /*
//  * UnknownMessageKeyException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class UnknownMessageKeyException : CommException
    {
        public UnknownMessageKeyException()
        {
        }

        public UnknownMessageKeyException(String msg)
            : base(msg)
        {
        }

        public UnknownMessageKeyException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public UnknownMessageKeyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}