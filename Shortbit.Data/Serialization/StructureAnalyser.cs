﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Format;
using Shortbit.Data.Serialization.Values;
using Shortbit.Utils;

namespace Shortbit.Data.Serialization
{
    public class StructureAnalyser<TS>
    {
        private static readonly Dictionary<Type, StructureAnalyser<TS>> Analysers = new Dictionary<Type, StructureAnalyser<TS>>();
        private static readonly object AnalysersSyncRoot = new object();

        public static StructureAnalyser<TS> GetDefault(Type type)
        {
            return GetDefault(type, DefaultFormatters.Get<TS>());
        }
        public static StructureAnalyser<TS> GetDefault(Type type, Func<Type, IFormatter<TS>> formatterCollection)
        {
            lock (AnalysersSyncRoot)
            {
                if (!Analysers.ContainsKey(type))
                    Analysers.Add(type, new StructureAnalyser<TS>(type, formatterCollection));
                return Analysers[type];
            }
        }


        public Type Type { get; set; }

        public Func<Type, IFormatter<TS>> FormatterCollection { get; set; }

        public Root<TS> Root { get; set; }

        public bool IsAnalysed { get; private set; } = false;

        private readonly Dictionary<Type, List<Attribute<TS>>> analysedComplexTypes = new Dictionary<Type, List<Attribute<TS>>>();
        private readonly object syncRoot = new object();

        public StructureAnalyser(Type type, Func<Type, IFormatter<TS>> formatterCollection)
        {
            this.Type = type;
            this.FormatterCollection = formatterCollection;
        }

        public void Analyse()
        {
            lock (this.syncRoot)
            {
                if (this.IsAnalysed)
                    return;

                this.Root = null;
                this.analysedComplexTypes.Clear();

                this.Root = CreateRoot(this.Type);
                this.IsAnalysed = true;
            }
        }

        private static bool IsPrimitiveType(Type t)
        {
            return PrimitiveTypes.Contains(t);
        }
        private static bool IsList(Type t)
        {
            if (typeof(Array).IsAssignableFrom(t))
                return true;

            return (t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IList<>)));
        }
        private static bool IsDictionary(Type t)
        {
            return
                (t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IDictionary<,>)));
        }

        private static bool IsValidProperty(PropertyInfo property)
        {
            return (!property.HasCustomAttribute<NotStoredAttribute>()
                    && property.CanRead
                    && property.CanWrite
                    && !property.SetMethod.IsStatic
                    && !property.GetMethod.IsStatic);
        }
        private static bool IsValidField(FieldInfo field)
        {
            return (!field.HasCustomAttribute<NotStoredAttribute>()
                    && !field.IsStatic);
        }

        public static Func<Object> CreateFactory(Type t)
        {

            if (t.IsValueType || t == typeof(string))
            {
                var eDefault = Expression.Default(t);
                var eCast = Expression.Convert(eDefault, typeof(object));
                var eLambda = Expression.Lambda<Func<Object>>(eCast);
                return eLambda.Compile();
            }
            var ctor = t.GetConstructor();
            if (ctor != null)
                return ctor.CreateDelegate<Func<Object>>();

            if (IsList(t))
            {
                ctor = t.GetConstructor(typeof(int));
                var del = ctor.CreateDelegate<Func<int, object>>();
                return () => del(0);
            }
            throw new ArgumentException("Can't create factory of Type "+t.FullName+". No default constructor found.");
        }

        private T CreatePropertyAttribute<T>(Type parentType, PropertyInfo property) where T : Attribute<TS>, new()
        {
            if (!IsValidProperty(property))
                return null;

            var eInstance = Expression.Parameter(typeof(object), "instance");
            var eInstanceCast = Expression.Convert(eInstance, parentType);

            var eValue = Expression.Parameter(typeof(object), "value");
            var eValueCast = Expression.Convert(eValue, property.PropertyType);

            var eGetterCall = Expression.Call(eInstanceCast, property.GetMethod);
            var eSetterCall = Expression.Call(eInstanceCast, property.SetMethod, eValueCast);

            var eGetterCast = Expression.Convert(eGetterCall, typeof(Object));

            var eGetterLambda = Expression.Lambda<Func<object, object>>(
                eGetterCast, eInstance);
            var eSetterLambda = Expression.Lambda<Action<object, object>>(eSetterCall, eInstance, eValue);

            var getter = eGetterLambda.Compile();
            var setter = eSetterLambda.Compile();


            var fullName = "";
            //if (instance != null)
            //    fullName = instance.FullName + ".";
            fullName += property.Name;

            var res = new T
            {
                Name = property.Name,
                FullName = fullName,
                Type = property.PropertyType,
                Getter = getter,
                Setter = setter
            };
            res.Factory = CreateFactory(res.Type);
            return res;
        }
        private T CreateFieldAttribute<T>(Type parentType, FieldInfo field) where T : Attribute<TS>, new()
        {
            if (!IsValidField(field))
                return null;

            var eInstance = Expression.Parameter(typeof(object), "instance");
            var eInstanceCast = Expression.Convert(eInstance, parentType);

            var eValue = Expression.Parameter(typeof(object), "value");
            var eValueCast = Expression.Convert(eValue, field.FieldType);

            var eField = Expression.Field(eInstanceCast, field);
            var eAssign = Expression.Assign(eField, eValueCast);

            var eFieldCast = Expression.Convert(eField, typeof(object));


            var eGetterLambda = Expression.Lambda<Func<object, object>>(
                eFieldCast, eInstance);
            var eSetterLambda = Expression.Lambda<Action<object, object>>(eAssign, eInstance, eValue);

            var getter = eGetterLambda.Compile();
            var setter = eSetterLambda.Compile();

            var fullName = "";
            //if (instance != null)
            //    fullName = instance.FullName + ".";
            fullName += field.Name;

            var res = new T
            {
                Name = field.Name,
                FullName = fullName,
                Type = field.FieldType,
                Getter = getter,
                Setter = setter
            };
            res.Factory = CreateFactory(res.Type);
            return res;
        }
        private T CreateAttribute<T>(Type parentType, MemberInfo member) where T : Attribute<TS>, new()
        {
            if (member is PropertyInfo)
                return CreatePropertyAttribute<T>(parentType, (PropertyInfo)member);
            return CreateFieldAttribute<T>(parentType, (FieldInfo)member);
        }
        private IEnumerable<Attribute<TS>> CreateAttributes(Type instanceType)
        {
            var props =
                instanceType.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(IsValidProperty).Select(p => Tuple.Create((MemberInfo)p, p.PropertyType));
            var fields = instanceType.GetFields(BindingFlags.Instance | BindingFlags.Public).Where(IsValidField).Select(f => Tuple.Create((MemberInfo)f, f.FieldType));

            var attributes = props.Concat(fields).ToList();

            attributes.Sort((t1, t2) => String.Compare(t1.Item1.Name, t2.Item1.Name, StringComparison.Ordinal));

            foreach (var tuple in attributes)
            {
                var m = tuple.Item1;
                var t = tuple.Item2;

                if (IsPrimitiveType(t))
                {
                    var value = CreateAttribute<PrimitiveAttribute<TS>>(instanceType, m);
                    PopulateSimpleValue(value, m);
                    yield return value;
                }
                else if (IsList(t))
                {
                    var value = CreateAttribute<CollectionAttribute<TS>>(instanceType, m);
                    PopulateCollectionValue(value, m);
                    yield return value;
                }
                else if (IsDictionary(t))
                {
                    var value = CreateAttribute<DictionaryAttribute<TS>>(instanceType, m);
                    PopulateDictionaryValue(value, m);
                    yield return value;
                }
                else
                {
                    var value = CreateAttribute<ComplexAttribute<TS>>(instanceType, m);
                    PopulateComplexValue(value);
                    yield return value;
                }
            }
        }


        private T CreateCollectionItem<T>(Type instanceType) where T : CollectionItem<TS>, new()
        {
            var itemType = instanceType.GenericTypeArguments[0];

            var eInstance = Expression.Parameter(typeof(object), "instance");
            var eInstanceCast = Expression.Convert(eInstance, instanceType);

            var eValue = Expression.Parameter(typeof(object), "value");
            var eValueCast = Expression.Convert(eValue, itemType);

            var eIndex = Expression.Parameter(typeof(int), "index");

            var indexProp = instanceType.GetProperty("Item", new[] { typeof(int) });


            var eGetterCall = Expression.Call(eInstanceCast, indexProp.GetMethod, eIndex);
            var eSetterCall = Expression.Call(eInstanceCast, indexProp.SetMethod, eIndex, eValueCast);

            var eGetterCast = Expression.Convert(eGetterCall, typeof(Object));

            var eGetterLambda = Expression.Lambda<Func<object, int, object>>(
                eGetterCast, eInstance, eIndex);
            var eSetterLambda = Expression.Lambda<Action<object, int, object>>(eSetterCall, eInstance, eIndex, eValue);

            var getter = eGetterLambda.Compile();
            var setter = eSetterLambda.Compile();

            var res = new T
            {
                Type = itemType,
                Getter = getter,
                Setter = setter,
                Factory = CreateFactory(itemType)
            };
            return res;
        }
        private CollectionItem<TS> CreateCollectionItem(Type instanceType, ICustomAttributeProvider attributeProvider)
        {
            var itemType = instanceType.GenericTypeArguments[0];

            if (IsPrimitiveType(itemType))
            {
                var value = CreateCollectionItem<PrimitiveCollectionItem<TS>>(instanceType);
                PopulateSimpleValue(value, attributeProvider);
                return value;
            }
            else if (IsList(itemType))
            {
                var value = CreateCollectionItem<CollectionCollectionItem<TS>>(instanceType);
                PopulateCollectionValue(value, attributeProvider);
                return value;
            }
            else if (IsDictionary(itemType))
            {
                var value = CreateCollectionItem<DictionaryCollectionItem<TS>>(instanceType);
                PopulateDictionaryValue(value, attributeProvider);
                return value;
            }
            else
            {
                var value = CreateCollectionItem<ComplexCollectionItem<TS>>(instanceType);
                PopulateComplexValue(value);
                return value;
            }
        }

        private T CreateDictionaryItem<T>(Type instanceType) where T : DictionaryItem<TS>, new()
        {
            var keyType = instanceType.GenericTypeArguments[0];
            var itemType = instanceType.GenericTypeArguments[1];

            var eInstance = Expression.Parameter(typeof(object), "instance");
            var eInstanceCast = Expression.Convert(eInstance, instanceType);

            var eValue = Expression.Parameter(typeof(object), "value");
            var eValueCast = Expression.Convert(eValue, itemType);

            var eKey = Expression.Parameter(typeof(object), "key");
            var eKeyCast = Expression.Convert(eKey, keyType);

            var indexProp = instanceType.GetProperty("Item", new[] { keyType });


            var eGetterCall = Expression.Call(eInstanceCast, indexProp.GetMethod, eKeyCast);
            var eSetterCall = Expression.Call(eInstanceCast, indexProp.SetMethod, eKeyCast, eValueCast);

            var eGetterCast = Expression.Convert(eGetterCall, typeof(Object));

            var eGetterLambda = Expression.Lambda<Func<object, object, object>>(
                eGetterCast, eInstance, eKey);
            var eSetterLambda = Expression.Lambda<Action<object, object, object>>(eSetterCall, eInstance, eKey, eValue);

            var getter = eGetterLambda.Compile();
            var setter = eSetterLambda.Compile();

            var res = new T
            {
                Type = itemType,
                Getter = getter,
                Setter = setter,
                Factory = CreateFactory(itemType)
            };
            return res;
        }
        private DictionaryItem<TS> CreateDictionaryItem(Type instanceType, ICustomAttributeProvider attributeProvider)
        {
            var itemType = instanceType.GenericTypeArguments[1];

            if (IsPrimitiveType(itemType))
            {
                var value = CreateDictionaryItem<PrimitiveDictionaryItem<TS>>(instanceType);
                PopulateSimpleValue(value, attributeProvider);
                return value;
            }
            else if (IsList(itemType))
            {
                var value = CreateDictionaryItem<CollectionDictionaryItem<TS>>(instanceType);
                PopulateCollectionValue(value, attributeProvider);
                return value;
            }
            else if (IsDictionary(itemType))
            {
                var value = CreateDictionaryItem<DictionaryDictionaryItem<TS>>(instanceType);
                PopulateDictionaryValue(value, attributeProvider);
                return value;
            }
            else
            {
                var value = CreateDictionaryItem<ComplexDicionaryItem<TS>>(instanceType);
                PopulateComplexValue(value);
                return value;
            }
        }


        private T CreateRoot<T>(Type instancetype) where T : Root<TS>, new()
        {
            return new T
            {
                Type = instancetype,
            };
        }

        private Root<TS> CreateRoot(Type instanceType)
        {
            // Since the root of serialization is a parameter for the serializer, it can't have attributes. So we use a empty default provider
            var attributeProvider = new RootAttributeProvider(); 

            if (IsPrimitiveType(instanceType))
            {
                var value = CreateRoot<PrimitiveRoot<TS>>(instanceType);
                PopulateSimpleValue(value, attributeProvider);
                return value;
            }
            else if (IsList(instanceType))
            {
                var value = CreateRoot<CollectionRoot<TS>>(instanceType);
                PopulateCollectionValue(value, attributeProvider);
                return value;
            }
            else if (IsDictionary(instanceType))
            {
                var value = CreateRoot<DictionaryRoot<TS>>(instanceType);
                PopulateDictionaryValue(value, attributeProvider);
                return value;
            }
            else
            {
                var value = CreateRoot<ComplexRoot<TS>>(instanceType);
                PopulateComplexValue(value);
                return value;
            }
        }

        private void PopulateSimpleValue(IPrimitiveValue<TS> value, ICustomAttributeProvider attributeProvider)
        {
            var t = value.Type;
            value.Formatter = this.FormatterCollection(t);

            if (attributeProvider != null && attributeProvider.HasCustomAttribute<FormatAttribute>())
            {
                foreach (var attrib in attributeProvider.GetCustomAttributes<FormatAttribute>())
                {
                    if (attrib.StoreType == t)
                    {
                        value.Formatter = (IFormatter<TS>)Activator.CreateInstance(attributeProvider.GetCustomAttribute<FormatAttribute>().Formatter);
                    }
                }
            }

        }

        private void PopulateComplexValue(IComplexValue<TS> value)
        {
            var t = value.Type;

            if (this.analysedComplexTypes.ContainsKey(t))
            {
                value.Attributes = this.analysedComplexTypes[t];
                return;
            }

            var attributes = new List<Attribute<TS>>();
            this.analysedComplexTypes.Add(t, attributes);
            value.Attributes = attributes;

            attributes.AddRange(CreateAttributes(t));

        }

        private void PopulateCollectionValue(ICollectionValue<TS> value, ICustomAttributeProvider attributeProvider)
        {
            var itemType = value.Type.GenericTypeArguments[0];
            value.ItemType = itemType;

            var wrapperType = typeof(ListWrapper<>).MakeGenericType(itemType);
            var listType = typeof(IList<>).MakeGenericType(itemType);

            var ciWrapperCtor = wrapperType.GetConstructor(listType);


            var eListParameter = Expression.Parameter(typeof(object), "list");
            var eListParameterCast = Expression.Convert(eListParameter, listType);
            var eCtorCall = Expression.New(ciWrapperCtor, eListParameterCast);
            var eCtorCallCast = Expression.Convert(eCtorCall, typeof(IList));
            var eCtorLambda = Expression.Lambda<Func<object, IList>>(eCtorCallCast, eListParameter);

            value.ItemsWrapper = eCtorLambda.Compile();

            value.Item = CreateCollectionItem(value.Type, attributeProvider);

        }

        private void PopulateDictionaryValue(IDictionaryValue<TS> value, ICustomAttributeProvider attributeProvider)
        {
            var keyType = value.Type.GenericTypeArguments[0];
            value.KeyType = keyType;
            var itemType = value.Type.GenericTypeArguments[1];
            value.ValueType = itemType;

            var wrapperType = typeof(DictionaryWrapper<,>).MakeGenericType(keyType, itemType);
            var dictionaryType = typeof(IDictionary<,>).MakeGenericType(keyType, itemType);

            var ciWrapperCtor = wrapperType.GetConstructor(dictionaryType);


            var eListParameter = Expression.Parameter(typeof(object), "dictionary");
            var eListParameterCast = Expression.Convert(eListParameter, dictionaryType);
            var eCtorCall = Expression.New(ciWrapperCtor, eListParameterCast);
            var eCtorCallCast = Expression.Convert(eCtorCall, typeof(IDictionary));
            var eCtorLambda = Expression.Lambda<Func<object, IDictionary>>(eCtorCallCast, eListParameter);

            value.ItemsWrapper = eCtorLambda.Compile();

            value.Value = CreateDictionaryItem(value.Type, attributeProvider);

        }

    }
}
