﻿// /*
//  * AquireLockTimeoutException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class AquireLockTimeoutException : CommException
    {
        public AquireLockTimeoutException()
        {
        }

        public AquireLockTimeoutException(String msg)
            : base(msg)
        {
        }

        public AquireLockTimeoutException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public AquireLockTimeoutException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}