﻿using System;
using System.Collections.Generic;
using MiscUtils.Numerics.Operators;
using MiscUtils.Numerics.ValueTypes;

namespace MiscUtils.Numerics.Math
{
	public class IterativeMean<T, OP> where OP : IArithmeticOperators<T>, ICast<T, Double>
	{
		private readonly LinkedList<T> buffer;
		private readonly OP operatorProvider;
		

		public T Mean { get; private set; }

		public int Size { get; private set; }

		public int Used
		{
			get { return this.buffer.Count; }
		}


		public IterativeMean(OP operatorProvider, int size)
		{
			this.operatorProvider = operatorProvider;
			this.buffer = new LinkedList<T>();
			this.Size = size;

			this.Mean = default(T);
			// TODO: Find a way to test the cast condition here...
		}

		public T Insert(T value)
		{
			while (this.Used >= this.Size)
				this.Remove();

			this.Add(value);


			return this.Mean;
		}


		public void Clear()
		{
			this.buffer.Clear();
			this.Mean = default(T);
		}


		private void Remove()
		{
			if (this.Used == 0) return;

			var value = this.buffer.First.Value;
			this.buffer.RemoveFirst();

			
			T n;
			T s;
			this.Cast(provider, out n, this.Used, out s, this.Used / (double)(this.Used - 1));

			this.Mean = this.Mean.Subtract(value.Divide(n)).Multiply(s);
			// self.mean = (self.mean - (_value / n)) * (n / (n-1))
		}

		private void Add(T value)
		{

			T n;
			T s;
			this.Cast(provider, out n, this.Used, out s, (this.Used - 1) / (double)this.Used);
			

			this.Mean = this.Mean.Multiply(s).Add(value.Divide(n));
			//self.mean = (n / (n+1)) * self.mean + (_value / (n+1))

			this.buffer.AddLast(value);
		}

		private void Cast(CastProvider<T> provider, out T n, double nIn, out T s, double sIn)
		{
			if (provider.CanCastFrom<Double>())
			{
				// ReSharper disable RedundantTypeArgumentsOfMethod
				n = provider.CastFrom<Double>(nIn);
				s = provider.CastFrom<Double>(sIn);
				// ReSharper restore RedundantTypeArgumentsOfMethod
			}
			else if (provider.CanCastFrom<NDouble>())
			{
				n = provider.CastFrom<NDouble>(nIn);
				s = provider.CastFrom<NDouble>(sIn);
			}
			else
				throw new InvalidOperationException("Given type T cannot be cast from Double or NDouble.");
		}
	}
}
