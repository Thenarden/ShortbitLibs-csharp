﻿using System;
using System.Xml;
using Shortbit.Data.Serialization.Reference;

namespace Shortbit.Data.Serialization.Values
{
    public abstract class DictionaryItem<TS> : IValue
    {
		public Type Type { get; set; }

		public Action<object, object, object> Setter { get; set; }
		public Func<object, object, object> Getter { get; set; }
		public Func<object> Factory { get; set; }

		public abstract void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, object key);
		public abstract void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, object key);
	}
}
