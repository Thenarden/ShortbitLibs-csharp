﻿// /*
//  * NotSupportedOperationException.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Data.Exceptions
{
    public class NotSupportedOperationException : Exception
    {
        public NotSupportedOperationException()
        {
        }

        public NotSupportedOperationException(String msg)
            : base(msg)
        {
        }

        public NotSupportedOperationException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public NotSupportedOperationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}