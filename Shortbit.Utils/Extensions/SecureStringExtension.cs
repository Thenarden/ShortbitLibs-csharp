﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace Shortbit.Utils
{
	public delegate void DataLoadDelegate(ref byte[] unprotectedData);


	public static class SecureStringExtension
	{

		public static T Process<T>(this SecureString self, Func<byte[], T> func)
		{
			IntPtr bstr = IntPtr.Zero;
			byte[] workArray = null;
			GCHandle? pinHandle = null;
			try
			{
				/*** PLAINTEXT EXPOSURE BEGINS HERE ***/
				bstr = Marshal.SecureStringToBSTR(self);

				workArray = new byte[self.Length * 2];
				pinHandle = GCHandle.Alloc(workArray, GCHandleType.Pinned);

				for (int i = 0; i < workArray.Length; i++)
					workArray[i] = Marshal.ReadByte(bstr, i);

				return func(workArray);
			}
			finally
			{
				if (workArray != null)
				{
					for (int i = 0; i < workArray.Length; i++)
						workArray[i] = 0;
				}
				pinHandle?.Free();


				if (bstr != IntPtr.Zero)
					Marshal.ZeroFreeBSTR(bstr);
				/*** PLAINTEXT EXPOSURE ENDS HERE ***/
			}
		}
		public static UnsafeByteArray TopByteArray<T>(this SecureString self)
		{
			IntPtr bstr = IntPtr.Zero;
			try
			{
				/*** PLAINTEXT EXPOSURE BEGINS HERE ***/
				bstr = Marshal.SecureStringToBSTR(self);

				var workArray = new byte[self.Length * 2];
				var pinHandle = GCHandle.Alloc(workArray, GCHandleType.Pinned);

				for (int i = 0; i < workArray.Length; i++)
					workArray[i] = Marshal.ReadByte(bstr, i);

				return new UnsafeByteArray(workArray, pinHandle);
			}
			finally
			{
				if (bstr != IntPtr.Zero)
					Marshal.ZeroFreeBSTR(bstr);
				/*** PLAINTEXT EXPOSURE ENDS HERE ***/
			}
		}

		public static UnsafeCharArray ToCharArray(this SecureString self)
		{
			var tmp = new byte[2];
			var res = new char[self.Length];
			var handle = GCHandle.Alloc(res, GCHandleType.Pinned);

			IntPtr passwordPtr = IntPtr.Zero;
			RuntimeHelpers.ExecuteCodeWithGuaranteedCleanup(
				delegate
				{
					RuntimeHelpers.PrepareConstrainedRegions();
					try
					{
					}
					finally
					{
						passwordPtr = Marshal.SecureStringToBSTR(self);
					}

					for (int i = 0; i < self.Length; i++)
					{
						tmp[0] = Marshal.ReadByte(passwordPtr, i * 2);
						tmp[1] = Marshal.ReadByte(passwordPtr, i * 2 + 1);
						res[i] = BitConverter.ToChar(tmp, 0);
					}
				},
				delegate
				{
					if (passwordPtr != IntPtr.Zero)
					{
						Marshal.ZeroFreeBSTR(passwordPtr);
					}
				},
				null);

			tmp[0] = 0;
			tmp[1] = 0;

			return new UnsafeCharArray(res, handle);
		}

		public static void Load(this SecureString self, DataLoadDelegate loader, int length)
		{

			var bytesworkArray = new byte[length];
			GCHandle bytesPinHandle = GCHandle.Alloc(bytesworkArray, GCHandleType.Pinned);

			try
			{

				loader(ref bytesworkArray);
				var ptr = bytesPinHandle.AddrOfPinnedObject();

				self.Clear();
				for (var i = 0; i < bytesworkArray.Length / 2; i++)
				{
					var c = BitConverter.ToChar(bytesworkArray, i * 2);
					self.AppendChar(c);
				}

			}
			finally
			{

				if (bytesworkArray != null)
				{
					for (int i = 0; i < bytesworkArray.Length; i++)
						bytesworkArray[i] = 0;
				}
				bytesPinHandle.Free();

			}
		}


		public static bool IsEqualTo(this SecureString self, SecureString other)
		{
			if (other == null)
			{
				throw new ArgumentNullException(nameof(other));
			}

			IntPtr bstr1 = IntPtr.Zero;
			IntPtr bstr2 = IntPtr.Zero;
			try
			{
				bstr1 = Marshal.SecureStringToBSTR(self);
				bstr2 = Marshal.SecureStringToBSTR(other);
				int length1 = Marshal.ReadInt32(bstr1, -4);
				int length2 = Marshal.ReadInt32(bstr2, -4);
				if (length1 == length2)
				{
					for (int x = 0; x < length1; ++x)
					{
						byte b1 = Marshal.ReadByte(bstr1, x);
						byte b2 = Marshal.ReadByte(bstr2, x);
						if (b1 != b2)
							return false;
					}
				}
				else
					return false;
				return true;
			}
			finally
			{
				if (bstr2 != IntPtr.Zero) Marshal.ZeroFreeBSTR(bstr2);
				if (bstr1 != IntPtr.Zero) Marshal.ZeroFreeBSTR(bstr1);
			}
		}

	}
}
