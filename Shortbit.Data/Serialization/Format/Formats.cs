﻿using System;
using System.Globalization;
using System.Text;
using Shortbit.Data.NBT;

namespace Shortbit.Data.Serialization.Values
{


	#region NBT Formatters
	

	internal class NBTFormat<T>: WrappedFormat<NBTTag, T>
	{
		public override NBTTag Format(T value)
		{
			return new NBTPrimitiveType<T>(value);
		}

		public override T Parse(NBTTag value)
		{
			return ((NBTPrimitiveType<T>)value).Value;
		}
	}
	#endregion
}
