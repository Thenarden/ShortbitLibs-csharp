﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Shortbit.Utils
{
	public static class ObservableCollectionExtension
	{
		public static void AddRange<T>(this ObservableCollection<T> self, IEnumerable<T> values)
		{
			foreach (var v in values)
			{
				self.Add(v);
			}
		}
	}
}
