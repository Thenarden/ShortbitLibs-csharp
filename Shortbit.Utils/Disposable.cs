﻿// /*
//  * Disposable.cs
//  *
//  *  Created on: 16:19
//  *         Author: 
//  */

using System;
using System.Threading;
using System.Threading.Tasks;
//using Shortbit.Utils.Compat;

namespace Shortbit.Utils
{
#if NETCOREAPP
	public abstract class Disposable : IDisposable, IAsyncDisposable
#else
	public abstract class Disposable : IDisposable
#endif
	{
		public Boolean IsDisposing { get; private set; }
		public Boolean IsDisposed { get; private set; }

		~Disposable()
		{
			this.RunDisposing(true);
		}
		
#if NETCOREAPP
		/// <inheritdoc />
		public ValueTask DisposeAsync()
		{
			this.Dispose();
			return new ValueTask();
		}
#endif

		public void Dispose()
		{
			this.RunDisposing(false);
		}


		private void RunDisposing(bool fromFinalizer)
		{
			if (this.IsDisposed || this.IsDisposing)
				return;

			this.IsDisposing = true;

			this.Dispose(fromFinalizer);
			if (!fromFinalizer) this.DisposeManaged();
			this.DisposeUnmanaged();

			this.IsDisposed = true;
			this.IsDisposing = false;
		}

		/// <summary>
		///     This method is used to call Dipose on any managed resource used by this class.
		///     NOTE: It is called after Dispose(bool fromFinalizer), but before DisposeUnmanaged() and only if not disposed by the
		///     finalizer!
		/// </summary>
		protected virtual void DisposeManaged()
		{
		}

		/// <summary>
		///     This method is used to dispose any unmanagd resources. It's called after Dispose(bool fromFinalizer) and
		///     DisposeManaged().
		/// </summary>
		protected virtual void DisposeUnmanaged()
		{
		}

		/// <summary>
		///     This is the unspecific dispose method. It's the first one to be called.
		/// </summary>
		/// <param name="fromFinalizer">
		///     True when the dispose routines get called from the finalizer of this class, otherwise false
		///     if via Dispose().
		/// </param>
		protected virtual void Dispose(bool fromFinalizer)
		{
		}
	}
}