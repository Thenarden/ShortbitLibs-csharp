﻿// /*
//  * EndianBinaryReader.cs
//  *
//  *  Created on: 01:03
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Shortbit.Utils.IO
{
	public class EndianBinaryReader : BinaryReader
	{
		public Endianess Endianess { get; private set; }


		public EndianBinaryReader(Stream input, Endianess endianess) : base(input)
		{
			Endianess = endianess;
		}

		public EndianBinaryReader(Stream input, Encoding encoding, Endianess endianess)
			: base(input, encoding)
		{
			Endianess = endianess;
		}

		private byte[] ReadBytesRequired(int byteCount)
		{
			var result = ReadBytes(byteCount);

			if (result.Length != byteCount)
				throw new EndOfStreamException(string.Format("{0} bytes required from stream, but only {1} returned.", byteCount,
					result.Length));

			return result;
		}


		public override bool ReadBoolean()
		{
			return EndianBitConverter.ToBoolean(ReadBytesRequired(sizeof (Boolean)), 0, Endianess);
		}


		public override char ReadChar()
		{
			return EndianBitConverter.ToChar(ReadBytesRequired(sizeof (Char)), 0, Endianess);
		}

		public override short ReadInt16()
		{
			return EndianBitConverter.ToInt16(ReadBytesRequired(sizeof (Int16)), 0, Endianess);
		}

		public override ushort ReadUInt16()
		{
			return EndianBitConverter.ToUInt16(ReadBytesRequired(sizeof (UInt16)), 0, Endianess);
		}

		public override int ReadInt32()
		{
			return EndianBitConverter.ToInt32(ReadBytesRequired(sizeof (Int32)), 0, Endianess);
		}

		public override uint ReadUInt32()
		{
			return EndianBitConverter.ToUInt32(ReadBytesRequired(sizeof (UInt32)), 0, Endianess);
		}

		public override long ReadInt64()
		{
			return EndianBitConverter.ToInt64(ReadBytesRequired(sizeof (Int64)), 0, Endianess);
		}

		public override ulong ReadUInt64()
		{
			return EndianBitConverter.ToUInt64(ReadBytesRequired(sizeof (UInt64)), 0, Endianess);
		}

		public override float ReadSingle()
		{
			return EndianBitConverter.ToSingle(ReadBytesRequired(sizeof (Single)), 0, Endianess);
		}

		public override double ReadDouble()
		{
			return EndianBitConverter.ToDouble(ReadBytesRequired(sizeof (Double)), 0, Endianess);
		}

		public override decimal ReadDecimal()
		{
			throw new NotImplementedException();
		}

		public override string ReadString()
		{
			throw new NotImplementedException();
		}

		public override char[] ReadChars(int count)
		{
			var ret = new List<char>();
			for (int i = 0; i < count; i++)
			{
				try
				{
					char read = ReadChar();
					ret.Add(read);
				}
				catch (EndOfStreamException)
				{
					break;
				}
			}
			return ret.ToArray();
		}
	}
}