﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Comm.Transport;

namespace Shortbit.Comm
{
	public class ConnectionCreatedEventArgs
	{
		public ConnectionCreatedEventArgs(Connection connection)
		{
			this.Connection = connection;
		}

		public Connection Connection { get; }
	}
}
