﻿// /*
//  * IComplement.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IComplement<T> : IOperator<T>
	{
		T Complement(ref T value);
	}
}