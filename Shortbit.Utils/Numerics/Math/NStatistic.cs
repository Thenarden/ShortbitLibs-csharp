﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiscUtils;
using MiscUtils.Numerics.Operators;

namespace MiscUtils.Numerics.Math
{
	public static class NStatistic
	{
		public static T Mean<T>(IEnumerable<T> list) where T : IAdd<T>, IDivide<T>, ICast<T>
		{
			var xs = list.ToListSafe();
			if (xs.Count == 0)
				return default(T);

			T sum = default(T);
			bool first = true;

			foreach (var x in xs)
			{
				if (first)
				{
					sum = x;
					first = false;
				}
				else
					sum.AssignAdd(x);
			}

			var provider = sum.GetCastProvider();
			sum.Divide(provider.CastFrom(xs.Count));

			return sum;
		}
	}
}
