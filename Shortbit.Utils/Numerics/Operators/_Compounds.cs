﻿// /*
//  * _Compounds.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System.Collections.Generic;

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IArithmeticOperators<T> : IAdd<T>, ISubtract<T>, IMultiply<T>, IDivide<T>, INegate<T>, IConstant<T>
	{
	}

	public interface INumericOperators<T> : IComparer<T>, IArithmeticOperators<T>, IDecrement<T>, IIncrement<T>,
		IRemainder<T>
	{
	}

	public interface IBinaryNumericOperators<T> : INumericOperators<T>, IComplement<T>, ILogicAnd<T>, ILogicOr<T>,
		ILogicXor<T>, IShift<T>
	{
	}
}