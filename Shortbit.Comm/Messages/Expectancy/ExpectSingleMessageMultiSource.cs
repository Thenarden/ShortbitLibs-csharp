﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Comm.Exceptions;
using Shortbit.Utils;
using Shortbit.Utils.Collections.Generic;

namespace Shortbit.Comm.Messages.Expectancy
{

	public abstract class ExpectSingleMessageMultiSourceBase<T> : MessageExpectancy
		where T : Message
	{
		private readonly Type[] messageTypes;
		private readonly Message responseMessage;
		private readonly HashSet<Guid> origins;
		private readonly HashSet<Guid> receivedOrigins = new HashSet<Guid>();

		private readonly Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator;

		private readonly ManualResetEvent terminated = new ManualResetEvent(false);
		public WaitHandle WaitHandle => this.terminated;

		public IReadOnlySet<Guid> ExpectedOrigins { get; }
		public IReadOnlySet<Guid> ReceivedOrigins { get; }
		
		private readonly Dictionary<Guid, T> receivedMessages = new Dictionary<Guid, T>();
		public IReadOnlyDictionary<Guid, T> ReceivedMessages { get; }


		internal ExpectSingleMessageMultiSourceBase(Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, Message responseMessage,
													Type[] messageTypes)
		{
			this.origins = new HashSet<Guid>(origins);
			this.responseMessage = responseMessage;
			this.messageTypes = messageTypes;
			this.terminator = terminator;

			this.ExpectedOrigins = new ReadOnlySet<Guid>(this.origins);
			this.ReceivedOrigins = new ReadOnlySet<Guid>(this.receivedOrigins);
			this.ReceivedMessages = new ReadOnlyDictionary<Guid, T>(this.receivedMessages);
		}

		public override bool IsExpected(Message message)
		{
			return this.messageTypes.Contains(message.GetType()) &&
				   (this.responseMessage == null || message.Header.IsResponseTo(this.responseMessage.Header)) &&
				   this.origins.Contains(message.Header.Origin) &&
				   !this.receivedMessages.ContainsKey(message.Header.Origin);
		}

		public override bool IsSatisfied()
		{
			if (this.terminator(this))
			{
				this.terminated.Set();
				return true;
			}

			return false;
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			this.terminated.Set();
		}

		public override void ProcessMessage(Message message)
		{
			this.receivedMessages[message.Header.Origin] = (T)message;
			this.receivedOrigins.Add(message.Header.Origin);
		}

	}

	public class ExpectSingleMessageMultiSourceSyncBase<T> : ExpectSingleMessageMultiSourceBase<T>
		where T : Message
	{
		
		public ExpectSingleMessageMultiSourceSyncBase(Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, Message responseMessage,
													  params Type[] messageTypes)
			: base(terminator, origins, responseMessage, messageTypes)
		{
		}


		public IReadOnlyDictionary<Guid, T> GetMessages()
		{
			this.WaitHandle.WaitOne();
			if (this.ThrowOnTimeout && this.TimedOut)
				throw ExpectancyTimeoutException.Create(this, this.ReceivedMessages);
			return this.ReceivedMessages;
		}
	}

	public class ExpectSingleMessageMultiSourceAsyncBase<T> : ExpectSingleMessageMultiSourceBase<T>
		where T : Message
	{
		private readonly TaskCompletionSource<IReadOnlyDictionary<Guid, T>> tcs = new TaskCompletionSource<IReadOnlyDictionary<Guid, T>>();
		
		private readonly CancellationTokenSource ownCts = new CancellationTokenSource();
		private readonly CancellationTokenSource cts;

		public ExpectSingleMessageMultiSourceAsyncBase(Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellationToken, Message responseMessage, 
													   params Type[] messageTypes)
			: base(terminator, origins, responseMessage, messageTypes)
		{
			this.cts = CancellationTokenSource.CreateLinkedTokenSource(this.ownCts.Token, cancellationToken);
			
			this.cts.Token.Register(() => this.tcs.TrySetCanceled(this.cts.Token) );
		}

		public override bool IsSatisfied()
		{
			var res = base.IsSatisfied();
			if (res)
				this.tcs.SetResult(this.ReceivedMessages);
			return res;
		}

		protected override void AfterDetach()
		{
			base.AfterDetach();
			if (!this.WaitHandle.WaitOne(0))
				this.ownCts.Cancel();
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			if (this.ThrowOnTimeout)
				this.tcs.SetException(ExpectancyTimeoutException.Create(this, this.ReceivedMessages));
			else
				this.tcs.SetResult(this.ReceivedMessages);
		}

		public Task<IReadOnlyDictionary<Guid, T>> GetMessagesAsync()
		{
			return this.tcs.Task;
		}

	}


	#region Typed Overloads
	public class ExpectSingleMessageMultiSource : ExpectSingleMessageMultiSourceSyncBase<Message>
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origins, responseTo, messageTypes)
		{ }
	}
	public class ExpectSingleMessageMultiSource<T1> : ExpectSingleMessageMultiSourceSyncBase<T1>
		where T1 : Message
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, responseTo, typeof(T1))
		{ }

	}
	public class ExpectSingleMessageMultiSource<T1, T2> : ExpectSingleMessageMultiSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectSingleMessageMultiSource<T1, T2, T3> : ExpectSingleMessageMultiSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectSingleMessageMultiSource<T1, T2, T3, T4> : ExpectSingleMessageMultiSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
	}
	public class ExpectSingleMessageMultiSource<T1, T2, T3, T4, T5> : ExpectSingleMessageMultiSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{
		public ExpectSingleMessageMultiSource(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}

	public class ExpectSingleMessageMultiSourceAsync : ExpectSingleMessageMultiSourceAsyncBase<Message>
	{
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origins, CancellationToken.None, responseTo, messageTypes)
		{ }
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origins, cancellation, responseTo, messageTypes)
		{ }

	}
	public class ExpectSingleMessageMultiSourceAsync<T1> : ExpectSingleMessageMultiSourceAsyncBase<T1>
		where T1 : Message
	{
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1))
		{ }
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origins, cancellation, responseTo, typeof(T1))
		{ }

	}
	public class ExpectSingleMessageMultiSourceAsync<T1, T2> : ExpectSingleMessageMultiSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator,
												   IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2))
		{ }

		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectSingleMessageMultiSourceAsync<T1, T2, T3> : ExpectSingleMessageMultiSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectSingleMessageMultiSourceAsync<T1, T2, T3, T4> : ExpectSingleMessageMultiSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }

	}
	public class ExpectSingleMessageMultiSourceAsync<T1, T2, T3, T4, T5> : ExpectSingleMessageMultiSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{ 

		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
			: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }
		public ExpectSingleMessageMultiSourceAsync(Func<ExpectSingleMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}
	#endregion
}
