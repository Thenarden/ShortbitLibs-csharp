﻿// /*
//  * ISubtract.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface ISubtract<T> : IOperator<T>
	{
		T Subtract(ref T a, ref T b);

		void AssignSubtract(ref T first, ref T other);
	}
}