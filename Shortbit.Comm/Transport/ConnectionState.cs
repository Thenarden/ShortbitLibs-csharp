﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Comm.Transport
{
	public enum ConnectionState
	{
		Idle,

		Handshaking,
		HandshakeSend,
		HandshakeDone,
		SettingUp,

		Maintenance,

		Active,
		Closing,
		Closed,
	}
}
