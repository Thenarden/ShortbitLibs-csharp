﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils
{
	public static class StreamExtension
	{
		public static void Write(this Stream self, byte[] buffer)
		{
			self.Write(buffer, 0, buffer.Length);
		}

		public static int Read(this Stream self, byte[] buffer)
		{
			return self.Read(buffer, 0, buffer.Length);
		}


		public static void ReadAll(this Stream self, byte[] buffer)
		{
			self.ReadAll(buffer, 0, buffer.Length);
		}

		public static void ReadAll(this Stream self, byte[] buffer, int index, int count)
		{
			int read = 0;

			while (read < count)
			{
				int r = self.Read(buffer, index + read, count - read);
				read += r;
			}
			
		}

	}
}
