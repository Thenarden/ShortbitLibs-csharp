﻿// /*
//  * Message.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Diagnostics;

namespace Shortbit.Comm.Messages
{
    public abstract class Message
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private MessageHeader header = new MessageHeader();

		public MessageHeader Header
		{
			get => this.header;
			internal set => this.header = value ?? throw new ArgumentNullException(nameof(value));
		}


		public abstract MessageDescriptor GetDescriptor();
		

		protected Message()
        {
        }
		
    }
}