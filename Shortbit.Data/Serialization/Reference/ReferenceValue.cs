﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization.Reference
{
    public class ReferenceValue
    {
        public IValue Value { get; set; }
        public object Instance { get; set; }
    }
}
