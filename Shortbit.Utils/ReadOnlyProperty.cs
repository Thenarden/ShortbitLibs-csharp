﻿// /*
//  * ReadOnlyProperty.cs
//  *
//  *  Created on: 03:54
//  *         Author: 
//  */

using System;

namespace Shortbit.Utils
{
	public class ReadOnlyProperty<T> : PropertyBase
	{
		public T Value
		{
			get { return this.getter(); }
		}

		public override object RawValue
		{
			get { return this.Value; }
		}

		private readonly Func<T> getter;

		public ReadOnlyProperty(T value)
			: this(() => value)
		{
		}

		public ReadOnlyProperty(Func<T> getter)
		{
			this.getter = getter;
		}

		public T Get()
		{
			return this.Value;
		}

		public static implicit operator T(ReadOnlyProperty<T> property)
		{
			if (property == null)
				return default(T);
			return property.Value;
		}
	}
}